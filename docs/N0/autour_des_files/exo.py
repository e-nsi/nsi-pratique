premiere_file = [42, 15, 64]
deuxieme_file = [22, 57, 36]
troisieme_file = [78, 12, 63]

#Enfiler l'élément 21 à la première file
...

#Défiler un élément à la première file
element_defile_1 = ...

#Défiler un nouvel élément à la première file
element_defile_2 = ...

#Défiler un élément de la première file
#et l'enfiler dans la deuxième file
...

#Défiler un élément de la deuxième file
#et l'enfiler dans la troisième file
...

#Défiler un élément de la troisième file
#et l'enfiler dans la première file
...
