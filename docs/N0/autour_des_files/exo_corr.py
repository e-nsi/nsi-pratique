premiere_file = [42, 15, 64]
deuxieme_file = [22, 57, 36]
troisieme_file = [78, 12, 63]

#Enfiler l'élément 21 à la file
premiere_file.append(21)

#Défiler un élément
element_defile_1 = premiere_file.pop(0)

#Défiler un nouvel élément
element_defile_2 = premiere_file.pop(0)

#Défiler un élément de la première pile
#et l'empiler dans la deuxième pile
element = premiere_file.pop(0)
deuxieme_file.append(element)

#Dépiler un élément de la deuxième pile
#et l'empiler dans la troisième pile
element = deuxieme_file.pop(0)
troisieme_file.append(element)

#Dépiler un élément de la troisième pile
#et l'empiler dans la première pile
element = troisieme_file.pop(0)
premiere_file.append(element)
