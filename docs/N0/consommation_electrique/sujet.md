---
author: Pierre Marquestaut
title: Consommation d'énergie
tags:
  - 0-simple
  - 4-fonctions
---

# Consommation d'énergie

On souhaite connaître la consommation d'énergie d'un appareil électrique, caractérisé par sa *tension* (exprimée en volt) et son *intensité* (exprimée en ampère).


La *puissance électrique* (exprimée en watt) consommée à chaque instant par un appareil est définie par : $\text{tension} \times \text{intensité}$

La puissance d'un four électrique de $230~\text{V}$ qui consomme $20~\text{A}$ est donc de $230 \times 20 = 4600~\text{W}$ 

**Créer** la fonction `puissance_appareil` qui calcule la puissance d'un appareil électrique en fonction de sa tension et de son intensité.

```python
>>> puissance_appareil(230, 20)
4600
```

L'*énergie consommée* (exprimée en watt-heure) par l'appareil pendant une durée donnée (exprimé en heure) est calculée par : $\text{puissance} \times \text{durée}$

Ainsi, l'utilisation du four pendant 1 h 30 min consommera $4600 \times 1.5 =  6900~\text{Wh}$


**Créer** la fonction `energie_consommee` qui calcule l'énergie d'un appareil électrique en fonction de sa puissance et du temps d'utilisation.

```python
>>> energie_consommee(4600, 1.5)
6900
```

En moyenne, le prix moyen de l'électricité est de 0,1582 €/kWh.

Ainsi, l'utilisation du four pendant 1 h 30 min coûtera environ 1,09 €, arrondi au centime près.

**Créer** la fonction `cout_utilisation` qui calcule le prix d'utilisation d'un appareil électrique en fonction de l'énergie consommée et du prix moyen.

```python
>>> cout_utilisation(6.9, 0.1582)
1.09
```

On utilisera la fonction `round` qui permet d'arrondir un nombre à une précision donnée.

```python
>>> round(4.52463, 3)
4.524
```

**Créer** la fonction `cout_consommation` qui calcule directement le prix de la consommation d'un appareil électrique de $230~\text{V}$  en fonction de l'intensité consommée et du temps d'utilisation. Le prix moyen de l'électricité sera fixé à 0,1582 €/kWh.
On réutilisera **impérativement** les fonctions déjà créées.


```python
>>> cout_consommation(0.3, 0.2)
1.09
```


{{ IDE('exo') }}
