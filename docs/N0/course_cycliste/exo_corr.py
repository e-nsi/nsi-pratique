def premier(classement):
    return classement[0]


def dernier(classement):
    dernière_position = len(classement)-1
    return classement[dernière_position]


def double(classement, position):
    assert position > 0, "Le premier ne peut doubler personne"
    assert position < len(classement), "Pas assez de coureurs"
    coureur_double = classement[position-1]
    classement[position-1] = classement[position]
    classement[position] = coureur_double


def crevaison(classement, position):
    assert position < len(classement), "Pas assez de coureurs"
    malchanceux = classement[position]
    while malchanceux != dernier(classement):
        double(classement, position+1)
        position += 1

