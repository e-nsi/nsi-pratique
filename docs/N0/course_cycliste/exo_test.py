course = ["Nadia", "Eric", "Thomas", "Elizabeth", "Laure"]
assert premier(course) == "Nadia"
assert dernier(course) == "Laure"
double(course, 3)
assert course == ["Nadia", "Eric", "Elizabeth", "Thomas", "Laure"]
crevaison(course, 1)
assert course == ["Nadia", "Elizabeth", "Thomas", "Laure", "Eric"]
