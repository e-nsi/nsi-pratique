def convertir_secondes(...):
    ...


# Tests
assert convertir_secondes((1, 25, 50)) == 5150
assert convertir_secondes((2, 20)) == 140

