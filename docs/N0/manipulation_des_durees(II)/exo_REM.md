{{ IDE('exo_corr') }}

Au lieu passer un p-uplet en unique paramètre, on aurait pu également passer les valeurs qu'il contient en paramètres de la fonction, le dernier (celui des `heures`) ayant une **valeur par défaut**.

```python
def convertir_secondes(secondes, minutes, heures = 0):
    return (heures * 60 + minutes) * 60 + secondes
```
Ainsi, on peut ensuite appeler la fonction avec trois arguments :

```pycon
>>> duree_en_hms = (1, 25, 50)
>>> valeur_heures, valeur_minutes, valeur_secondes = duree_en_hms
>>> convertir_secondes(valeur_secondes, valeur_minutes, valeur_heures)
5150
```
ou avec seulement deux arguments :

```pycon
>>> duree_en_ms = (2, 20)
>>> valeur_minutes, valeur_secondes = duree_en_ms
>>> convertir_secondes(valeur_secondes, valeur_minutes)
140
```
Dans ce dernier cas, le paramètre `heures` prend la valeur par défaut `0`.
