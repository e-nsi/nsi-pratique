def convertir_secondes(duree):
    if len(duree) == 2:
        minutes, secondes = duree
        return minutes * 60 + secondes
    else:
        h, m, s = duree
        return (h * 60 + m) * 60 + s
