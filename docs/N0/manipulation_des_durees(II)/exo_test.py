# Tests
assert convertir_secondes((1, 25, 50)) == 5150
assert convertir_secondes((2, 20)) == 140

# autres tests
assert convertir_secondes((1, 0, 0)) == 3600
assert convertir_secondes((1, 1, 1)) == 3661
assert convertir_secondes((0, 1, 0)) == 60
assert convertir_secondes((0, 0, 1)) == 1
assert convertir_secondes((1, 1)) == 61
assert convertir_secondes((1, 0)) == 60
assert convertir_secondes((0, 1)) == 1

assert convertir_secondes((0, 0, 0)) == 0
assert convertir_secondes((0, 0)) == 0
