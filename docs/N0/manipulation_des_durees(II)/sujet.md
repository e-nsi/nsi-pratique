---
author: Pierre Marquestaut
title: Les durées(II)
tags:
  - 0-simple
  - 2-tuple
---

# Les durées(II)

Les durées peuvent être exprimées en secondes, en minutes-secondes, ou en heures-minutes-secondes.

Ainsi, la durée 6 h 34 min et 12 s peut être exprimée par :
```python
duree_s = 21612          # en secondes
duree_min_sec = (360, 12)     # en minutes-secondes
duree_hms = (6, 34, 12)   # en heures-minutes-secondes
```

On souhaite créer une fonction permettant de convertir une durée en secondes, quelque soit son expression initiale.


**Créer** la fonction `convertir_secondes` qui prend en paramètre une durée exprimée soit en heures, minutes et secondes, soit en minutes et secondes, et qui renvoie le nombre total de secondes.

```pycon
>>> convertir_secondes((1, 25, 50))
5150
>>> convertir_secondes((2, 20))
140
```

??? note "Rappels sur les opérateurs `//` et `%`" 

    L'opérateur `//` permet d'effectuer la division entière entre deux nombres.

    ```python
    >>> 10 // 4
    2
    ```

    L'opérateur `%` permet de trouver le reste de la division entière entre deux nombres.
    
    ```python
    >>> 10 % 3
    1
    ```

??? note "Rappel sur la fonction `len`" 

    La fonction `len` permet de connaître le nombre d'éléments contenus dans un type construits (tableau, dictionnaire, p-uplet...).
    
    ```python
    >>> chaine = "Bonjour"
    >>> len(chaine)
    7
    ```
{{ IDE('exo') }}
