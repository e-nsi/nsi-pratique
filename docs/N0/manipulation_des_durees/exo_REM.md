{{ IDE('exo_corr') }}

La correction proposée repose sur le procédé de *unpacking*, qui consiste à affecter les valeurs d'un tuple dans différentes variables et ce, en une seule instruction.

Ainsi, les instructions suivantes :

```python
heures = duree[0]
minutes = duree[1]
secondes = duree[2]
```

sont équivalente à celle-là :

```python
heures, minutes, secondes = duree
```
Dans ce cas, il faut naturellement qu'il y ait autant de variables que de valeurs à affecter.
