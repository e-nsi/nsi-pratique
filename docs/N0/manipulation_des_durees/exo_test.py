assert convertir_secondes((1, 25, 50)) == 5150
assert convertir_min_sec(125) == (2, 5)
assert convertir_h_m_s(125) == (0, 2, 5)

assert convertir_min_sec(600) == (10, 0)
assert convertir_min_sec(602) == (10, 2)
assert convertir_h_m_s(7200) == (2, 0, 0)
assert convertir_h_m_s(7300) == (2, 1, 40)


assert convertir_secondes((1, 0, 0)) == 3600
assert convertir_secondes((1, 1, 1)) == 3661
assert convertir_secondes((0, 1, 0)) == 60
assert convertir_secondes((0, 0, 1)) == 1

assert convertir_h_m_s(3600) == (1, 0, 0)
assert convertir_h_m_s(60) == (0, 1, 0)
assert convertir_h_m_s(1) == (0, 0, 1)

assert convertir_h_m_s(0) == (0, 0, 0)
assert convertir_h_m_s(0) == (0, 0, 0)
assert convertir_secondes((0, 0, 0)) == 0
