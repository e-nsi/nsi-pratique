---
author: Pierre Marquestaut
title: Les durées
tags:
  - 0-simple
  - 2-tuple
---

# Les durées

Les durées peuvent être exprimées en secondes, en minutes-secondes, ou en heures-minutes-secondes.

Ainsi, la durée 6 h 34 min et 12 s peut être exprimée par :
```python
duree_s = 23652          # en secondes
duree_min_sec = (394, 12)     # en minutes-secondes
duree_hms = (6, 34, 12)   # en heures-minutes-secondes
```

On souhaite créer des fonctions permettant d'exprimer les durées selon différentes expressions.

!!! note "Rappels : l'opérateur `//`"

    L'opérateur `//`permet d'effectuer la division entière entre deux nombres.

    ```python
    >>> 10 // 4
    2
    ```

!!! note "Rappels : l'opérateur `%`"

    L'opérateur `%` permet de trouver le reste de la division entière entre deux nombres.

    ```python
    >>> 10 % 3
    1
    ```
**Créer** la fonction `convertir_secondes(duree)` qui prend en paramètre une `duree` (de type `tuple` et exprimée en heures, minutes et secondes) et qui renvoie le nombre total de secondes.

```python
>>> duree_a_convertir = (1, 25, 50)
>>> convertir_secondes(duree_a_convertir)
5150
```
**Créer** la fonction `convertir_min_sec` qui prend en paramètre une durée exprimée en secondes et qui renvoie la durée exprimée en minutes et secondes.

```python
>>> convertir_min_sec(125)
(2, 5)
```
**Créer** la fonction `convertir_h_m_s` qui prend en paramètre une durée exprimée en secondes et qui renvoie la durée exprimée en heures, minutes et secondes.

```python
>>> convertir_h_m_s(125)
(0, 2, 5)
```

{{ IDE('exo') }}
