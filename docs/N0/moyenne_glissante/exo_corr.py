def filtre(valeurs, nouvelle_mesure):

    # Calcul de la valeur filtrée par moyenne glissante
    valeur_filtrée = (valeurs[0] + valeurs[1] + valeurs[2] + valeurs[3]
                      + nouvelle_mesure)/5

    # Modification de la fenêtre
    valeurs[0], valeurs[1], valeurs[2] = valeurs[1], valeurs[2], valeurs[3]
    valeurs[3] = nouvelle_mesure

    return valeurs, valeur_filtrée
