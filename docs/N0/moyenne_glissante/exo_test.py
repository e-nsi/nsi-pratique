fenetre = [5, 7, 7, 9]
fenetre, valeur = filtre(fenetre, 10)
assert fenetre == [7, 7, 9, 10]
assert round(valeur, 1) == 7.6
fenetre, valeur = filtre(fenetre, 12)
assert fenetre == [7, 9, 10, 12]
assert round(valeur, 1) == 9
fenetre, valeur = filtre(fenetre, 12)
assert fenetre == [9, 10, 12, 12]
assert round(valeur, 1) == 8
fenetre, valeur = filtre(fenetre, 15)
assert fenetre == [10, 12, 12, 15]
assert round(valeur, 1) == 11.6
