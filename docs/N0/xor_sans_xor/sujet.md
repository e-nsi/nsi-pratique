---
author: Vincent Bouillot
title: ou exclusif sans XOR
tags:
  - 2-booléen
---

# ou exclusif sans XOR

Ce site internet est bâti grâce à un générateur de site web statique appelé MkDocs. MkDocs permet de paramétrer deux palettes de couleur : le mode `dark` et le mode `light`. 

Pour suivre les changements de palette de MkDocs, l'éditeur de code de ce site  détecte les clics sur le bouton ☀️, repéré par un identificateur HTML :

|☀️ caché|☀️ visible|
|-|-|
|éditeur `tomorrow`| éditeur `crimson`|

Lors de la création de son site statique, on spécifie dans un fichier appelé `mkdocs.yml` quelle est la palette par défaut. Cela fixe les identificateurs pour les boutons 🌙 et ☀️ :

|Identificateur|`dark`|`light`|
|-|-|-|
|`palette_1`|🌙|☀️|
|`palette_2`|☀️|🌙|

Le problème est le suivant. Le code Javascript utilise seulement l'identificateur `palette_1`.

On souhaite donc réaliser une fonction qui permet de changer la couleur de l'éditeur selon la tableau ci-dessous :

|mode par défaut|`palette_1` caché|`palette_1` visible|
|-|-|-|
|`dark` |éditeur `tomorrow`| éditeur `crimson`|
|`light`|éditeur `crimson`| éditeur `tomorrow`|

Chacun des nombres utilisés est de type `int` ou `float`.

> :warning: On interdit ici d'utiliser `max`, ainsi que `sort` ou `sorted`.

!!! example "Exemples"

    ```pycon
    >>> maximum([98, 12, 104, 23, 131, 9])
    131
    >>> maximum([-27, 24, -3, 15])
    24
    ```

{{ IDE('exo', SANS = "max") }}
