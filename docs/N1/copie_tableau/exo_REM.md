Une autre solution possible en utilisant une boucle :
```python
def copie_superficielle(tableau: list):
    """
    Créer un tableau qui est la copie d'un tableau initial
    """
    taille = len(tableau)
    nouveau = [0]*taille  
    for i in range(taille):
        nouveau[i] = tableau[i]
    return nouveau
```
Cependant, la copie réalisée ici est qualifiée de **superficielle** : on a bien crée un nouveau tableau, dans une nouvelle zone mémoire, mais on a seulement copié les références des éléments contenus dans le tableau initial.

Cette solution est donc insuffisante lorsqu'il s'agira de copier un tableau de tableaux :
![copie_tab](copie_tab.svg){ width=25% }
```pycon
>>> valeurs = [[10, 11, 12], [14, 15, 16]]
>>> copie = copie_superficielle(valeurs)
>>> valeurs[1][1] = 0
>>> copie
[[10, 11, 12], [14, 0, 16]]
```
La fonction a bien créé deux tableaux distincts, mais les tableaux qu'ils contiennent sont communs aux deux. Ainsi quand on modifie un élément de `valeurs`, il est également changé dans `copie`

