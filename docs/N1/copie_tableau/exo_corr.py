def copie_superficielle(tableau: list):
    """
    Créer un tableau qui est la copie d'un tableau initial
    """
    nouveau = [element for element in tableau]
    return nouveau
