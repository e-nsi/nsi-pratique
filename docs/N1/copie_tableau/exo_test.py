tableau_1 = [1, 2, 3]
tableau_2 = copie_superficielle(tableau_1)
tableau_1[0] = 0
assert tableau_2 == [1, 2, 3]
tableau_2[1] = 15
assert tableau_1 == [0, 2, 3]
tableau_1 = []
tableau_2 = copie_superficielle(tableau_1)
assert tableau_2 == []
tableau_1 = [1]
tableau_2 = copie_superficielle(tableau_1)
assert tableau_2 == [1]
tableau_1 = ["papa", "maman", "la bonne", "moi"]
tableau_2 = copie_superficielle(tableau_1)
assert tableau_2 == ["papa", "maman", "la bonne", "moi"]
