---
author: Pierre Marquestaut
title: Copie d'un tableau
tags:
  - 1-boucle
---

# Copie d'un tableau



Lorsque l'on souhaite réaliser une copie d'un tableau, le premier réflexe est de procéder par affectation :

```pycon
>>> valeurs = [10, 11, 12]
>>> copie = tableau
>>> copie
[10, 11, 12]
```
Par cette méthode, on ne crée pas une copie du tableau, mais on obtient deux variables qui désignent le même tableau : on parle alors de **copie de référence** ou **par alias**. 
![affectation](affectation.svg){ width=25% }

Ainsi la modification d'un élément de la variable `copie` entrainera la modification de ce même élément dans `valeurs`.

```pycon
>>> copie[1] = 0
>>> copie
[10, 0, 12]
>>> valeurs
[10, 0, 12]

```
**Écrire** une fonction `copie_superficielle` qui crée la copie d'un tableau passé en attribut. Pour ce faire, on créera un nouveau tableau et on y affectera un à un les éléments du tableau initial.

```pycon
>>> valeurs = [10, 11, 12]
>>> copie = copie_superficielle(valeurs)
>>> valeurs[1] = 0
>>> valeur
[10, 0, 12]
>>> copie
[10, 11, 12]
>>> valeurs = []
>>> copie = copie_superficielle(valeurs)
>>> valeurs 
[]
```
![copie](copie.svg){ width=25% }

{{ IDE('exo') }}
