def premier(classement):
    """renvoie le dossard du coureur en première position de la course."""
    ...

def dernier(...):
    """renvoie le dossard du coureur en dernière position de la course."""
    ...

def position(...):
    """renvoie la position d'un coureur identifié par son dossard"""
    ...
      
def double(...):
    """modifie le tableau avec le coureur passé en paramètre qui est passé devant le coureur précédent."""
    ...
    
def crevaison(...):
    """
    modifie le tableau avec le coureur passé en paramètre qui est passé en dernière position
    après avoir été doublé par tous les autres coureurs.
    """
    ...

course = ["Nadia-01", "Franck-64", "Thomas-31", "Elizabeth-22", "Laure-66"]
assert premier(course) == "Nadia-01"
assert dernier(course) == "Laure-66"
assert position(course, "Elizabeth-22") == 3
double(course, "Thomas-31")
assert course == ["Nadia-01", "Thomas-31", "Franck-64", "Elizabeth-22", "Laure-66"]
crevaison(course, "Franck-64")
assert course == ["Nadia-01", "Thomas-31", "Elizabeth-22", "Laure-66", "Franck-64"]
