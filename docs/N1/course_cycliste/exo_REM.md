{{ IDE('exo_corr') }}

On pouvait également opter pour la solution suivante :
```python
def dernier(classement):
    """renvoie le dossard du coureur en dernière position de la course."""
    return classement[-1]
```
