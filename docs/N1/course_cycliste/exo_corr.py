def premier(classement):
    """renvoie le dossard du coureur en première position de la course."""
    return classement[0]


def dernier(classement):
    """renvoie le dossard du coureur en dernière position de la course."""
    derniere_position = len(classement) - 1
    return classement[derniere_position]

def position(classement, dossard):
    """renvoie la position d'un coureur identifié par son dossard"""
    for position_coureur in range(len(classement)):
        if classement[position_coureur] == dossard:
            return position_coureur
      

def double(classement, dossard):
    """
    modifie le tableau avec le coureur (passé en paramètre) 
    qui s'est positionné devant le coureur précédent.
    """
    position_coureur = position(classement, dossard)
    coureur_devant = classement[position_coureur - 1]
    classement[position_coureur - 1] = classement[position_coureur]
    classement[position_coureur] = coureur_devant
    return classement


def crevaison(classement, dossard):
    """
    modifie le tableau avec le coureur passé en paramètre qui est passé en dernière position
    après avoir été doublé par tous les autres coureurs.
    """
    position_coureur = position(classement, dossard)
    while dossard != dernier(classement):
        precedent = classement[position_coureur + 1]
        double(classement, precedent)
        position_coureur += 1
    return classement
