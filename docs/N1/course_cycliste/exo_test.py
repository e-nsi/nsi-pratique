course = ["Nadia-01", "Franck-64", "Thomas-31", "Elizabeth-22", "Laure-66"]
assert premier(course) == "Nadia-01"
assert dernier(course) == "Laure-66"
assert position(course, "Elizabeth-22") == 3
double(course, "Thomas-31")
assert course == ["Nadia-01", "Thomas-31", "Franck-64", "Elizabeth-22", "Laure-66"]
crevaison(course, "Franck-64")
assert course == ["Nadia-01", "Thomas-31", "Elizabeth-22", "Laure-66", "Franck-64"]
crevaison(course, "Franck-64")
assert course == ["Nadia-01", "Thomas-31", "Elizabeth-22", "Laure-66", "Franck-64"]

course = ["moi"]
assert premier(course) == "moi"
assert dernier(course) == "moi"
assert position(course, "moi") == 3
crevaison(course, "moi")
assert course == ["moi"]
