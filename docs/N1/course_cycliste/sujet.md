---
author: Pierre Marquestaut
title: La course cycliste
tags:
  - 1-boucle
---

# La course cycliste

Une course cycliste se dispute. 

![course](https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/V%C3%A9locourse.jpg/220px-V%C3%A9locourse.jpg) <br/>
*source :* [Wikipedia](https://commons.wikimedia.org/wiki/File:V%C3%A9locourse.jpg)

Chaque cycliste est identifié par un numéro de dossard : `"Nadia-01"`, `"Franck-64"`

 Les cyclistes se doublent les uns les autres durant la course.

Il peut arriver qu'un cycliste crève, et se fasse doubler par tous les autres coureurs et se retrouve à la dernière place, puis repart.

Les cyclistes sont rangés dans un tableau suivant leur position dans la course.

Exemple :
```python
course = ["Nadia-01", "Franck-64", "Thomas-31", "Elizabeth-22", "Laure-66"]
```
Dans cet exemple, Nadia est la première et Laure la dernière.

* **Écrire** la fonction `premier` qui prend en paramètre le tableau qui stocke la course et qui renvoie le coureur en première position de la course. 
```pycon
>>> premier(course)
'Nadia-01'
```

* **Écrire** la fonction `dernier` qui prend en paramètre le tableau qui stocke la course et qui renvoie le coureur en dernière position de la course.
```pycon
>>> dernier(course)
'Laure-66'
```

* **Écrire** la fonction `position` qui prend en paramètres le tableau qui stocke la course et le numéro de dossard et qui renvoie sa position dans la course.  On supposera que le dossard donné est bien présent dans le classement.
**Attention** : cette position correspond à l'indice du coureur dans le tableau. (ainsi le premier de la course aura la position 0 et non 1)
```pycon
>>> position(course, "Nadia-01")
0
>>> position(course, "Thomas-31")
2
```

* **Écrire** la fonction `double` qui prend en paramètres le tableau qui stocke la course et le dossard d'un coureur et modifie puis renvoie le tableau avec le coureur qui est passé devant le coureur précédent.

```pycon
>>> double(course, "Thomas-31")
['Nadia-01', 'Thomas-31', 'Franck-64', 'Elizabeth-22', 'Laure-66']
```

* **Écrire** la fonction `crevaison` qui prend en paramètres le tableau qui stocke la course et le dossard d'un coureur qui a crevé et modifie puis renvoie le tableau. On partira du principe que tous les coureurs derrière le malchanceux coureur le doublent jusqu'à ce que le coureur soit le dernier. 
On utilisera **obligatoirement** les fonctions `double` et `dernier`.


```pycon
>>> crevaison(course, "Thomas-31")
['Nadia-01', 'Franck-64', 'Elizabeth-22', 'Laure-66', 'Thomas-31']
```



{{ IDE('exo') }}
