#--- HDR ---#
def trier_valeurs(dico):
    for cle, valeur in dico.items():
        dico[cle] = sorted(valeur)
    return dico


#--- HDR ---#
def inverser(dico):
    dico_inverse = {}
    for cle, valeur in dico.items():
        if valeur not in dico_inverse:
            dico_inverse[valeur] = [cle]
        else:
            dico_inverse[valeur].append(cle)
    return dico_inverse


# tests
dico = {"a": 5, "b": 7}
assert trier_valeurs(inverser(dico)) == {5: ["a"], 7: ["b"]}

dico = {"a": 5, "b": 7, "c": 5}
assert trier_valeurs(inverser(dico)) == {5: ["a", "c"], 7: ["b"]}

dico = {
    "Paris": "Tour Eiffel",
    "Rome": "Colisée",
    "Berlin": "Reichtag",
    "Londres": "Big Ben",
}
assert trier_valeurs(inverser(dico)) == {
    "Tour Eiffel": ["Paris"],
    "Colisée": ["Rome"],
    "Reichtag": ["Berlin"],
    "Big Ben": ["Londres"],
}

dico = {"Paris": "P", "Lyon": "L", "Nantes": "N", "Lille": "L"}
assert trier_valeurs(inverser(dico)) == {
    "P": ["Paris"],
    "L": ["Lille", "Lyon"],
    "N": ["Nantes"],
}
