def costume(numero, couleurs):
    ...

# Tests
assert costume(1, ["bleu", "blanc", "rouge"]) == "bleu"
assert costume(3, ["bleu", "blanc", "rouge"]) == "rouge"
assert costume(8, ["bleu", "blanc", "rouge"]) == "blanc"

