def costume(numero, couleurs):
    n = len(couleurs)
    rang = numero % n - 1
    return couleurs[rang]
