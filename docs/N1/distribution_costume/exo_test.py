# Tests
assert costume(1, ["bleu", "blanc", "rouge"]) == "bleu"
assert costume(3, ["bleu", "blanc", "rouge"]) == "rouge"
assert costume(8, ["bleu", "blanc", "rouge"]) == "blanc"
assert costume(1000, ["bleu", "blanc", "rouge"]) == "bleu"
assert costume(300, ["bleu", "blanc", "rouge"]) == "rouge"