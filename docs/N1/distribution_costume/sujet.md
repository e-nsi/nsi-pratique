---
author: Mireille Coilhac
title: Distribution de costumes
tags:
  - 4-fonctions
---
# Distribuer des costumes pour un spectacle

La mairie d'une ville a décidé d'organiser un spectacle en plein air pour le 14 juillet. Pour cela, elle va faire appel à de nombreux figurants, et distribuer à chacun d'eux un costume bleu, ou blanc, ou rouge. Ne sachant pas à l'avance combien de personnes vont se présenter, l'organisateur du spectacle a décidé, afin d'équilibrer à peu près les couleurs, de procéder de la façon suivante : le premier figurant arrivé recevra un costume bleu, le second un blanc, le troisième un rouge, le quatrième un bleu, le cinquième un blanc, etc.  
Compléter la fonction `costume` ci-dessous. Elle prend en paramètre `numero` qui est le numéro d'arrivée du figurant, et `couleurs` qui est la liste des couleurs des costumes. Elle doit renvoyer la couleur du costume donné au figurant correspondant.
On garantit que la liste `couleurs` n'est pas vide, et que `numero` est un entier supérieur ou égal à 1.

Par exemple : 

```pycon
>>> costume(2, ["bleu", "blanc", "rouge"])
'blanc'
```


{{ IDE('exo') }}
