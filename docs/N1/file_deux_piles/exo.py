#--- HDR ---#
def creer_pile_vide():
    '''Renvoie une liste vide'''
    return ()
 
def est_pile_vide(p):
    '''Renvoie True si la pile est vide'''
    return p == ()
 
def empiler(elt, p) :
    '''
    Renvoie une nouvelle pile oÃ¹ elt est le sommet
    et pile la suite de notre nouvelle pile.
    '''
    return (elt, p)
 
def lire_sommet(p):
    '''Renvoie la valeur stockÃ©e au sommet d'une pile NON VIDE'''
    return p[0]
 
def depiler(p):
    '''
    Renvoie une nouvelle pile oÃ¹ on a supprimÃ© l'ancien sommet
    d'une pile NON VIDE
    '''
    return p[1]
#--- HDR ---#


def creer_file_vide():
    '''Renvoie une file vide'''
    ...

def est_vide(file):
    '''Renvoie True si la file est vide'''
    ...

def enfiler(file, valeur):
    '''Ajoute l'élément valeur à la file'''
    ...
    
def defiler(file):
    '''
    Défile un élément de la file
    Renvoie cet élément
    '''
    ...

# Tests
ma_file = creer_file_vide()
enfiler(ma_file, 1)
enfiler(ma_file, 2)
enfiler(ma_file, 3)
enfiler(ma_file, 4)
enfiler(ma_file, 5)
assert defiler(ma_file) == 1
assert defiler(ma_file) == 2

