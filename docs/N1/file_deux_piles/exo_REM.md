{{ IDE('exo_corr') }}

Dans cet exercice, on utilise une structure de pile dont les opérations d'empilement et de dépilement se font à coût constant $O(1)$.

Qu'en est-il de la structure de file constituée de deux piles ?

* L'opération d'**enfilement** consiste à empiler l'élément dans la pile d'entrée. Cette opération se fait donc à coût constant.
* L'opération de **défilement** revient à réaliser trois opérations : dépiler l'élément de la pile d'entrée, l'empiler dans la pile de sortie puis le dépiler de cette même pile. Ces trois opérations se font chacune à coût constant et donc l'opération de défilment est également à coût constant.

Ainsi, on a pu créer une structure de file dont les opérations d'enfilement et de défilement se font à coût constant $O(1)$.

