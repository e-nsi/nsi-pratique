def creer_file_vide():
    '''Renvoie une file vide'''
    return [creer_pile_vide(), creer_pile_vide()]

def est_file_vide(file):
    '''Renvoie True si la file est vide'''
    return est_pile_vide(file[0]) and est_pile_vide(file[1])

def enfiler(file, valeur):
    '''Ajoute l'élément valeur à la file'''
    file[0] = p.empiler(valeur, file[0])

def defiler(file):
    '''
    Défile un élément de la file
    Renvoie cet élément
    '''
    if p.est_pile_vide(file[1]):
        while not p.est_pile_vide(file[0]):
            valeur = p.lire_sommet(file[0])
            file[0] = p.depiler(file[0])
            file[1] = p.empiler(valeur, file[1])
    avant = p.lire_sommet(file[1])
    file[1] = p.depiler(file[1])
    return avant

