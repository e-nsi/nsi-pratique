---
author: Pierre Marquestaut
title: File avec deux piles
tags:
  - 3-struct.lineaire
---

# File avec deux piles

On choisit d'implémenter une `file` à l'aide d'un couple `[p1,p2]` où `p1` et `p2` sont des piles. Ainsi `file[0]` et `file[1]` sont respectivement les piles `p1` et `p2`.

Pour enfiler un nouvel élément `valeur` dans `file`, on l'empile dans `p1`.

![enfiler](file2pile1.svg){ width=25% }

Pour défiler `file`, deux cas se présentent :
* La pile `p2` n'est pas vide : on dépile `p2`.

![enfiler](file2pile2.svg){ width=25% }

* La pile `p2` est vide : on dépile les éléments de `p1` en les empilant dans `p2` jusqu'à ce que `p1` soit vide, puis on dépile `p2`.

![enfiler](file2pile3.svg){ width=25% }

La structure de pile est utilisable au travers de leur interface constituée des fonctions suivantes :
* `creer_pile_vide` : renvoie une pile vide
* `depiler` : prend en paramètre une `pile` et renvoie une nouvelle pile où on a supprimé l'ancien sommet
* `empile` : prend un `element` quelconque et une `pile` et renvoie une nouvelle pile où `element` est le sommet.
* `est_pile_vide` : prend en paramètre une `pile` et renvoie True si la pile est vide
* `lire_sommet` : prend en paramètre une `pile` et renvoie la valeur stockée au sommet d'une pile NON VIDE

```pycon
>>> ma_pile = creer_pile_vide()
>>> est_pile_vide(ma_pile)
True
>>> empiler(5, ma_pile)
>>> empiler(4, ma_pile)
>>> lire_sommet(ma_pile)
4
>>> depile(ma_pile)
```

A partir des opérations sur les piles fournis dans le module, **écrire** en Python les fonctions suivantes :
  * une fonction `creer_file_vide()` qui renvoie une `file` vide de tout élément.
  * une fonction `est_vide(file)` qui prend en argument une `file` et qui renvoie `True` si la `file` est vide, `False` sinon.
  * une fonction `enfiler(file, valeur)` qui prend en arguments une `file` et un élément `valeur` et qui ajoute `valeur` en queue de la `file`.
  * une fonction `defiler(file)` qui prend en argument une `file` et qui renvoie l'élément en tête de la `file` en le retirant.

On doit utiliser exclusivement les fonctions fournies. Aucune connaissance de la manière dont sont stockées les valeurs de la pile n'est nécessaire.
{{ IDE('exo') }}

