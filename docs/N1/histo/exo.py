def histo(liste_ent):
    ...

liste_ent_1 = [1, 1, 1, 3, 2, 1, 2 ,5]
liste_ent_2 = [5, 5, 5, 0, 7]

assert histo(liste_ent_1) == [0, 4, 2, 1, 0, 1]
assert histo(liste_ent_2) == [1, 0, 0, 0, 0, 3, 0, 1]
assert histo([]) == []





