class Cellule():
    def __init__(self):
        self.info = None
        self.suivante = None

def contient_boucle(tete: Cellule) -> bool:
    ...  # À compléter


# Tests

# liste test_1 : 18 cellules avec boucle, celle de l'énoncé
liste_test_A = Cellule()
liste_test_A.info = 0

case = liste_test_A
for i in range(1, 18):
    suite = Cellule()
    suite.info = i
    if i == 13:
        debut_boucle = suite
    case.suivante = suite
    case = suite
case.suivante = debut_boucle 

assert contient_boucle(liste_test_A) is True

# liste liste_test_B : 8 cellules, sans boucle
liste_test_B = Cellule()
liste_test_B.info = 0

case = liste_test_B
for i in range(1, 8):
    case.suivante = Cellule()
    case = case.suivante
    case.info = i

assert contient_boucle(liste_test_B) is False

# liste liste_test_C : liste vide
liste_test_C = Cellule()

assert contient_boucle(liste_test_C) is False


