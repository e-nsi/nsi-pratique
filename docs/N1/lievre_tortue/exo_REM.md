## Commentaires

{{ IDE('exo_corr') }}

## Pourquoi ça marche ? 

De manière générale, nous savons que lorsque `tortue` a effectué $x$ pas, `lievre` en a effectué $2x$. De plus, à partir d'une des cellules comprises dans la boucle, si `lievre` fait un tour complet, il se retrouve sur la même cellule. 

Appelons $\lambda$ le nombre de cellules dans la boucle. Dans notre exemple, $\lambda = 5$.

Lorsque `tortue` arrive sur la première cellule de la boucle dont le numéro est un multiple de $\lambda$ (`tortue.info` est donc de la forme $k \lambda$ où $k$ est entier non nul), `lievre` a effectué $2 k \lambda = k  \lambda + k \lambda$ pas. Autrement dit, il a effectué $k$ tours depuis la cellule numéro $k \lambda$ où se trouve actuellement la `tortue`, ce qui fait que `tortue` == `lièvre`.

Dans notre exemple, $k = 3$.
