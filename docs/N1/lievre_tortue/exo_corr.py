def contient_boucle(tete):
    """
    Détermine si la liste chainée contient une boucle,
      la première cellule est `tete`,
      grâce à l'algorithme de Floyd.
    Renvoie un booléen.
    """
    lievre = tortue = tete
    while lievre is not None and lievre.suivante is not None:
        tortue = tortue.suivante
        lievre = lievre.suivante.suivante
        if lievre == tortue:
            return True
    return False
