# à refaire ; facile, presque une copie des premiers tests
# mettre des tests avec des cellules qui ont la même info

# liste test_1 : 18 cellules avec boucle, celle de l'énoncé
test_1 = Cellule()
test_1.info = 0

case = test_1
for i in range(1, 18):
    suite = Cellule()
    suite.info = i
    if i == 13:
        debut_boucle = suite
        case.suivante = suite
        case = suite
        case.suivante = debut_boucle 

# liste test_2 : 8 cellules, sans boucle
test_2 = Cellule()
test_2.info = 0

case = test_2
for i in range(1, 8):
    case.suivante = Cellule()
    case = case.suivante
    case.info = i

# liste test_3 : liste vide
test_3 = Cellule()

# liste test_4 : une boucle globale
test_4 = Cellule()
test_4.info = 0

case = test_4
for i in range(1, 8):
    case.suivante = Cellule()
    case = case.suivante
    case.info = i

case.suivante = test_4

# liste test5 : 2 cellules en boucle
test_5 = Cellule()
test_5.info = 1
test_5.suivante = Cellule()
test_5.suivante.info = 2
test_5.suivante.suivante = test_5


# liste test_6 : 7 cellules sans boucle
prec = test_6 = Cellule()
test_6.info = 0
for i in range(1, 7):
    prec.suivante = Cellule()
    prec.suivante.info = i
    prec = prec.suivante


# liste test_7 : 7 cellules de même info sans boucle
prec = test_7 = Cellule()
test_7.info = 7
for i in range(1, 7):
    prec.suivante = Cellule()
    prec.suivante.info = "7"
    prec = prec.suivante

assert contient_boucle(test_1) is True
assert contient_boucle(test_2) is False
assert contient_boucle(test_3) is False
assert contient_boucle(test_4) is True
assert contient_boucle(test_5) is True
assert contient_boucle(test_6) is False
assert contient_boucle(test_7) is False
