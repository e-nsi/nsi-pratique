---
author: François Meyer
title: Mme Tortue et M. Lièvre font la course
tags:
  - 3-struct.lineaire
forum: https://mooc-forums.inria.fr/moocnsi/t/exercice-sur-les-listes-chainees/7540
---


# Listes chainées : Mme Tortue et M. Lièvre font la course

Dans une liste chainée, la dernière cellule n'a pas de `suivante`. Si c'était le cas, sa `suivante` serait une des cellules précédentes, ce qui formerait une **boucle**. Le parcours d'une telle liste ne se terminerait jamais. 

Cet exercice consiste à réaliser une fonction telle que `contient_boucle(tete: Cellule)` détermine si la liste chainée dont la première cellule est `tete` contient une boucle, en renvoyant un booléen.

Dans cet exercice, une cellule de liste chainée est une instance de la classe `Cellule` ci-dessous.

```python
class Cellule():
    def __init__(self):
        self.info = None
        self.suivante = None
```

Voici un exemple de liste chainée comportant une boucle. 

=== "Graphe"

    ```mermaid
    graph LR;
        0[0]-->1[1];
        1[1]-->2[2];
        2[2]-- ... de 3 à 10 ... -->11[11];
        11[11]-->12[12];
        12[12]-->13[13];
        13[13]-->14[14];
        14[14]-->15[15];
        15[15]-->16[16];
        16[16]-->17[17];
        17[17]-->13[13]
    ```

=== "Version texte"

        0 → 1 → ... 12 → 13 → 14 → 15 
                         ↑          ↓
                         17  ←--   16

(les numéros correspondent aux valeurs de l'attribut `info` de chaque cellule)

Et un programme python permettant de la construire : 

```python linenums="1"
test_1 = Cellule()
test_1.info = 0

case = test_1
for i in range(1, 18):
    suite = Cellule()
    suite.info = i
    if i == 13:
        debut_boucle = suite
    case.suivante = suite
    case = suite

case.suivante = debut_boucle
# la suivante de la n°17 devient la n°13
```

Étudiez bien ce programme pour comprendre la suite et testez vos modifications pour bien comprendre la structure de la liste.


!!! info "Détection de boucle"
    Il existe un moyen très simple de détecter une boucle.

    Supposons deux variables `lièvre` et `tortue` qui prennent comme valeurs des cellules successives d'une liste chainée, en commençant par la `tête`, mais de sorte que `lièvre` passe d'une cellule directement à *la suivante de sa suivante*, alors que `tortue` passe seulement d'une cellule à sa suivante. Autrement dit, `lièvre` fait deux pas quand `tortue` n'en fait qu'un. 

    Dans notre exemple, on représente sur ce tableau les positions respectives de  `lièvre` et `tortue` en écrivant leur attribut `info`. Le tableau s'arrête lorsque `tortue == lievre`.

    <table>
        <tr>
        <td><code>tortue.info</code></td>
        <td>0</td>
        <td>1</td>
    <td>2</td>
    <td>3</td>
    <td>4</td>
    <td>5</td>
    <td>6</td>
    <td>7</td>
    <td>8</td>
    <td>9</td>
    <td>10</td>
    <td>11</td>
    <td>12</td>
    <td>13</td>
    <td>14</td>
    <td>15</td>
    </tr>
    <tbody>
    <tr>
    <td><code>lievre.info</code></td>
    <td>0</td>
    <td>2</td>
    <td>4</td>
    <td>6</td>
    <td>8</td>
    <td>10</td>
    <td>12</td>
    <td>14</td>
    <td>16</td>
    <td>13</td>
    <td>15</td>
    <td>17</td>
    <td>14</td>
    <td>16</td>
    <td>13</td>
    <td>15</td>
    </tr>
    <tr>
    <td>nb de pas du <code>lievre</code></td>
    <td>0</td>
    <td>2</td>
    <td>4</td>
    <td>6</td>
    <td>8</td>
    <td>10</td>
    <td>12</td>
    <td>14</td>
    <td>16</td>
    <td>18</td>
    <td>20</td>
    <td>22</td>
    <td>24</td>
    <td>26</td>
    <td>28</td>
    <td>30</td>
    </tr>
    </tbody>
    </table>

    **Lorsque `lièvre` entre dans la boucle, il se met à tourner en rond. Au bout d'un moment, `tortue` le rattrape et se trouve sur la même cellule que lui. Quand la condition `lièvre == tortue` devient vraie, nous savons qu'il y a une boucle.**

    Cet algorithme est dû à Robert Floyd.

**Écrivez la fonction** qui détermine la présence d'une boucle.

!!! tip "Indices"

    1. Penser à ajouter une docstring et d'autres tests unitaires.
    2. Attention au point de départ : si lièvre et tortue partent de la même cellule, `lievre == tortue`
    3. Attention à la fin de course : si la tortue rejoint le lièvre, ou si le lièvre arrive en fin de liste (sans boucle)


{{ IDE('exo') }}

