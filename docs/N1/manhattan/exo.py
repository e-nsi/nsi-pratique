def distance(A, B):
    ...


def prochaine_livraison(livraisons, position_actuelle):
    ...


# Tests
A = (0, 1)
B = (2, 3)
assert distance(A, B) == 4

livraisons_1 = [(2, 0), (0, 1), (3, 3), (2, 3)]
assert prochaine_livraison(livraisons_1, (0, 0)) == (0, 1)

livraisons_2 = [(2, 0), (3, 3), (2, 3)]
assert prochaine_livraison(livraisons_2, (0, 1)) == (2, 0)

livraisons_3 = [(3, 3), (2, 3)]
assert prochaine_livraison(livraisons_3, (2, 0)) == (2, 3)

livraisons_4 = [(3, 3)]
assert prochaine_livraison(livraisons_4, (2, 3)) == (3, 3)

livraisons_5 = [(1, 0), (0, 1)]
assert prochaine_livraison(livraisons_5, (0, 0)) == (1, 0)