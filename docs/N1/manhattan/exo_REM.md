# Commentaires

{{ IDE('exo_corr') }}

Il s'agit en réalité d'une recherche de minimum.

On peut aussi noter qu'en effectuant successivement différentes recherches, le livreur peut ainsi obtenir l'ensemble du trajet qu'il doit effectuer afin de livrer l'ensemble de ses colis.

Cette recherche de trajet se fait en choisissant à chaque étape la prochaine livraison la plus proche de la position actuelle. Facile à metre en œuvre, cette méthode ne garantit pas que la longueur totale du trajet soit la plus minimale.

