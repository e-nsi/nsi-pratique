def distance(A, B):
    xA, yA = A
    xB, yB = B
    return abs(xB - xA) + abs(yB - yA)


def prochaine_livraison(livraisons, position_actuelle):
    plus_proche = livraisons[0]
    distance_minimale = distance(position_actuelle, plus_proche)
    for i in range(1, len(livraisons)):
        prochain = livraisons[i]
        distance_prochain = distance(position_actuelle, prochain)
        if distance_prochain < distance_minimale:
            plus_proche = prochain
            distance_minimale = distance_prochain
    return plus_proche


# Tests
A = (0, 1)
B = (2, 3)
assert distance(A, B) == 4
livraisons_1 = [(2, 0), (0, 1), (3, 3), (2, 3)]
assert prochaine_livraison(livraisons_1, (0, 0)) == (0, 1)
livraisons_2 = [(2, 0), (3, 3), (2, 3)]
assert prochaine_livraison(livraisons_2, (0, 1)) == (2, 0)
livraisons_3 = [(3, 3), (2, 3)]
assert prochaine_livraison(livraisons_3, (2, 0)) == (2, 3)
livraisons_4 = [(3, 3)]
assert prochaine_livraison(livraisons_4, (2, 3)) == (3, 3)
livraisons_5 = [(1, 0), (0, 1)]
assert prochaine_livraison(livraisons_5, (0, 0)) == (1, 0)
