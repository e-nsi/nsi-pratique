---
author: Nicolas Revéret
title: Livraisons à Manhattan
tags:
    - boucle
status: relecture
---

# Livraisons à Manhattan

> On n'utilisera pas `min`, `sort` ou `sorted` dans cet exercice.


Un livreur new-yorkais organise sa tournée. Le quartier dans lequel il évolue est un quadrillage :
toutes les rues suivent l'axe Nord ↔ Sud ou l'axe Est ↔ Ouest.

Les adresses de ses livraisons sont donc des couples de coordonnées, `#!py (2, 3)` par exemple. La première coordonnée est l'*abscisse* du point, la seconde son *ordonnée*.

Ces adresses de livraison sont données dans une liste python. Par exemple :

```python
livraisons = [(2, 0), (0, 1), (3, 3), (2, 3)]
```

Ce qui correspond à la carte ci-dessous :

![La carte](carte.svg){.autolight}

Afin de déterminer son itinéraire, il décide de toujours livrer en priorité l'adresse la plus "proche" de sa position actuelle. Se déplaçant le long des rues, il mesure la distance $AB$ entre deux points de coordonnées $(x_A\,; y_A)$ et $(x_B\,; y_B)$ en faisant :

$$AB = \text{abs}(x_B-x_A)+\text{abs}(y_B-y_A)$$

où $\text{abs}(x)$ est la valeur absolue du nombre $x$ : $\text{abs}(3-5)=\text{abs}(5-3)=2$.

!!! tip "Coup de pouce"
    Avec Python, `#!py abs(x)` renvoie la valeur absolue de `x`

Vous devez écrire deux fonctions :

* une fonction `distance(A, B)` calculant la distance entre deux points dont on fournit les coordonnées
* une fonction `prochaine_livraison(livraisons, position_actuelle)` prenant en argument la liste des adresses de livraison et la position actuelle du livreur et renvoyant les coordonnées du prochain point à livrer

Si deux points sont à la même distance de la position actuelle du livreur on renverra les coordonnées du premier point rencontré dans la liste.

!!! tip "Coup de pouce"
    Python permet de récupérer facilement les différents valeurs d'un tuple grâce au *unpacking*. Par exemple :

    ```pycon
    >>> x, y = (3, 8)
    >>> x
    3
    >>> y
    8
    ```

!!! example "Exemples"

    ```pycon
    >>> A = (0, 1)
    >>> B = (2, 3)
    >>> distance(A, B)
    4
    >>> livraisons_1 = [(2, 0), (0, 1), (3, 3), (2, 3)]
    >>> prochaine_livraison(livraisons_1, (0, 0))
    (0, 1)
    >>> livraisons_2 = [(2, 0), (3, 3), (2, 3)]
    >>> prochaine_livraison(livraisons_2, (0, 1))
    (2, 0)
    >>> livraisons_3 = [(3, 3), (2, 3)]
    >>> prochaine_livraison(livraisons_3, (2, 0))
    (2, 3)
    >>> livraisons_4 = [(3, 3)]
    >>> prochaine_livraison(livraisons_4, (2, 3))
    (3, 3)
    >>> livraisons_5 = [(1, 0), (0, 1)]
    >>> prochaine_livraison(livraisons_5, (0, 0))
    (1, 0)
    ```

{{ IDE('exo') }}
