def poids_boite(boite):
    total = 0
    for objet in boite:
        total += objet

    return total


def rangement(poids_objets, poids_max):
    boites = []

    for objet in poids_objets:
        indice = 0
        ajout = False
        while not ajout and indice < len(boites):
            if poids_boite(boites[indice]) + objet <= poids_max:
                boites[indice].append(objet)
                ajout = True
            else:
                indice += 1
        if not ajout:
            boites.append([objet])

    return boites


# Tests
assert poids_boite([]) == 0
assert poids_boite([7, 5, 3, 1]) == 16
assert rangement([7, 5, 3, 1], 8) == [[7, 1], [5, 3]]
assert rangement([7, 5, 3, 1], 16) == [[7, 5, 3, 1]]
