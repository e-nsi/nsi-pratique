def moyenne_bourree(notes):
    nb_juges = len(notes)
    assert nb_juges > 2, "Pas assez de juges"
    mini = 10
    maxi = 0
    total = 0
    for note in notes:
        total += note
        if note < mini:
            mini = note
        if note > maxi:
            maxi = note
    return (total - mini - maxi) / (nb_juges - 2)

