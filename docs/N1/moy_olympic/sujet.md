---
author: Guillaume CONNAN, Pierre Marquestaut
title: Moyenne olympique
tags:
    - 1-boucle
---

# Moyenne olympique

Le championnat d'Europe de la bourrée auvergnate va avoir lieu à Nantes cette
année. Chaque athlète reçoit une note du panel  de $n$ juges (il y en a au moins
3).
![bourée](https://upload.wikimedia.org/wikipedia/commons/5/5a/Bourr%C3%A9e_d%27Auvergne.jpg) <br/>
*Domaine public*

La note finale est calculée selon la **moyenne olympique**, c'est-à-dire en enlevant les deux notes extrêmes et en calculant la moyenne des $n - 2$ notes qui restent.

Vous  devez aider  les  organisateurs à  donner  la note  finale  en créant  une fonction  `moyenne_bourree(notes)` où  `notes` est  un tableau contenant les  notes des juges (les notes sont des nombres entiers ou à virgule flottante entre 0 et 10).


**CONTRAINTE** : on n'utilisera pas les fonctions Python `max`, `min`, ou `sum`.

!!! example "Exemples"

    ```pycon
	>>> notes1 = [2, -1, 2, 10, 2]
	>>>  moy_bourree(notes1)
    2.0
	>>> notes2 = [1, 1, 1, 1, 1, 1]
	>>> moy_bourree(notes2)
    1.0
	>>> notes3 = [1, 2]
	>>> moy_bourree(notes3)
    AssertionError: Pas assez de juges
    ```

{{ IDE('exo') }}
