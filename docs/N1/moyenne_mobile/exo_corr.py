def somme(serie):
    s = 0
    for element in serie:
        s = s + element
    return s


def moyenne(serie):
    return somme(serie)/len(serie)


def moyenne_mobile(serie):
    N = 8
    if len(serie) < N:
        return []
    resultat = []
    for indice in range(N - 1, len(serie)):
        sous_serie = [serie[indice - N + i] for i in range(N)]
        resultat = resultat + [moyenne(sous_serie)]
    return resultat
