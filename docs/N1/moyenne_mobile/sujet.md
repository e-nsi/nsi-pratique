---
author: Vincent-Xavier Jumel
title: Moyenne glissante

---

Dans le carde d'une série de mesure issue d'un micro-contrôleur, on récupère des valeurs avec une grande variabilité, ou avec des imperfections (on parle de bruit).

Dans le cadre d'un micro-contrôleur, on va tâcher d'éviter de manipuler des nombres à virgule flotante (`float`) et privilégier des entiers (`int`). En effet, si on fait la moyenne sur quatre entiers, les résultats seront d'une des quatre formes suivantes :
* `x.00`
* `x.25`
* `x.50`
* `x.75`

!!! remarque Décimation

    Cette méthode s'appelle la décimation. En particulier, additionner 4 nombres de $M$ bits nécessite $M$+2 bits pour le résultat.

Le souci de ces moyennes, c'est que pour qu'elle soit fiable, il faudrait la calculer sur 64 ou 128 valeurs, ce qui est souvent pas réalisable.

!!! remarque Pourquoi diviser par une puissance de 2 ?

    On divise par une puissance de 2 dans le calcul de la moyenne, car cela correspond à un simple décalage à droite des bits et c'est donc très rapide.

Si on fait la moyenne sur trop peu de valeurs d'un phénomène chaotique, on prend la moyenne sur $N$ valeurs pour l'extrapoler à $M$ valeurs. Par exemple, dans la bourse, cela reviendrai à prendre la moyenne journalière et à l'extrapoler comme moyenne hebdomadaire.

![](rh_wk.png) ![](rh_mm50.png)

On va donc effectuer une moyenne mobile sur les $N$ dernières valeurs, avec par exemple $N = 8$.

Écrire une fonction `moyenne_mobile` qui renvoie les moyennes sur les 8
dernières occurences de la série. Si la liste passée en argument est de
longueur inférieure à 8, on renvoie `[]`.

> On n'utilisera ni `sum`, ni `mean` ni les tranches (`liste[i:i+7]`)

!!! example "Exemples"

    ```pycon
    >>> moyenne_mobile([1,1,1,1,1,1])
    []
    >>> moyenne_mobile([1,1,1,1,1,1,1])
    [1.0]
    >>> moyenne_mobile([1,2,3,4,5,6,7,8,9,10,11,12]
    [4.714285714285714, 4.0, 5.0, 6.0, 7.0, 8.0]
    ```

{{ IDE('exo') }}
