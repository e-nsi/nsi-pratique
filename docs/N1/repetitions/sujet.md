---
author: Mireille Coilhac
title: Nombre de répétitions
tags:
  - 1-boucle
--- 

Écrire une fonction python appelée `repetitions` qui prend en paramètres un élément `cible` et un tableau de `valeurs` et renvoie le nombre de fois où l'élément `cible` apparait dans le tableau `valeurs`.

> Il est **interdit** d'utiliser la méthode `count`

!!! example "Exemples"

    ```python
    >>> repetitions(5, [2, 5, 3, 5, 6, 9, 5])
    3
    >>> repetitions('A', ['B', 'A', 'B', 'A', 'R'])
    2
    >>> repetitions(12, [1, 7, 21, 36, 44])
    0
    ```

{{ IDE('exo', SANS='count') }}
