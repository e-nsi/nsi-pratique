# Tests
# Quels sont les noms des animaux volants ?
assert requete_1 == [animal[0] for animal in animaux if animal[5]]

# Combien y-a-t'il d'animaux aquatiques sans pattes ?
assert requete_2 == ([animal[0] for animal in animaux if animal[6] and animal[13] == 0])

# Quels sont les noms des animaux aquatiques sans pattes ?
assert requete_3 == [animal[0] for animal in animaux if animal[6] and animal[13] == 0]

# Quels sont les noms des prédateurs ?
assert requete_4 == [animal[0] for animal in animaux if animal[7]]

# Combien y-a-t'il d'animaux sans plumes ?
assert requete_5 == len([animal[0] for animal in animaux if animal[2]])

# Quels sont les noms des animaux ayant plus de 5 pattes (exclu) ?
assert requete_6 == [animal[0] for animal in animaux if animal[13] > 5]

# Combien y-a-t'il de prédateurs volants ?
assert requete_7 == len([animal[0] for animal in animaux if animal[5] and animal[7]])

# Chercher toutes les informations de la cigale (chaîne `'cigale`)
assert requete_8 == [animal for animal in animaux if animal[1] == "cigale"]

# Combien y-a-t'il d'animaux domestiques, de la taille d'un chat et ne pondant des oeufs ?
assert requete_9 == [animal for animal in animaux if animal[15] and animal[16] and animal[3]]

# Chercher le nombre de pattes du papillon ? (extraire le résultat de la liste)
assert requete_10 == [animal[13] for animal in animaux if animal[1] == "cigale"][0]
