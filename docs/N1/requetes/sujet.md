---
author: Nicolas Revéret
title: Requêtes dans une liste de listes
tags:
  - 2-booléen
  - 4-grille
---
# Effectuer des requêtes dans une liste de données

On s'intéresse dans cet exercice aux données décrivant différents animaux ([source](https://www.kaggle.com/datasets/agajorte/zoo-animals-extended-dataset?resource=download&select=zoo2.csv)).

Chaque animal est décrit par les attributs suivants :

|   Attributs   |           Description           | Type Python | Indice |
| :-----------: | :-----------------------------: | :---------: | :----: |
|     `nom`     |             Son nom             | `#!py str`  |   `0`    |
|    `poil`     |       A-t-il des poils ?        | `#!py bool` |   `1`    |
|    `plume`    |       A-t-il des plumes ?       | `#!py bool` |   `2`    |
|    `oeuf`     |       Pond-il des œufs ?        | `#!py bool` |   `3`    |
|    `lait`     |    Allaite-t-il ses petits ?    | `#!py bool` |   `4`    |
|   `volant`    |         Peut-il voler ?         | `#!py bool` |   `5`    |
|  `aquatique`  |       Vit-il dans l'eau ?       | `#!py bool` |   `6`    |
|  `prédateur`  |      Est-ce un prédateur ?      | `#!py bool` |   `7`    |
|    `dents`    |       A-t-il des dents ?        | `#!py bool` |   `8`    |
|  `vertébré`   |      Est-ce un vertébré ?       | `#!py bool` |   `9`    |
|     `air`     |    Respire-t-il dans l'air ?    | `#!py bool` |   `10`   |
|  `venimeux`   |        Est-il venimeux ?        | `#!py bool` |   `11`   |
|  `nageoires`  |     A-t-il des nageoires ?      | `#!py bool` |   `12`   |
|   `pattes`    |   Combien a-t-il de pattes ?    | `#!py int`  |   `13`   |
|    `queue`    |       A-t-il une queue ?        | `#!py bool` |   `14`   |
| `domestique`  |  Est-ce un animal domestique ?  | `#!py bool` |   `15`   |
| `taille_chat` | Est-il de la taille d'un chat ? | `#!py bool` |   `16`   |

Ces données ont été importées dans une liste Python nommée `animaux`. Chacun des éléments de cette liste est une liste contenant les informations décrivant un animal. On affiche ci-dessous le premier élément de la liste `animaux` :

```pycon
>>> animaux[0]  # l'ensemble des informations
['tortue', False, False, True, False, False, True, False, False, True, True, False, False, 4, True, True, True]
>>> animaux[0][0]  # le nom de l'animal
'tortue'
>>> animaux[0][1]  # cet animal a-t-il des poils ?
False
>>> animaux[0][6]  # cet animal vit-il dans l'eau ?
True
```

Comme on peut le voir ci-dessus, cet élément décrit la tortue (valeur à l'indice `0`). La tortue n'a pas de poils (valeur `#!py False` à l'indice `1`) et vit dans l'eau (valeur `#!py True` à l'indice `6`).

Le but de l'exercice est d'effectuer des requêtes sur cette liste afin de sélectionner certains éléments (certains animaux). On utilise pour ce faire les listes en compréhension.

L'instruction ci-dessous permet par exemple de sélectionner les noms (attribut `nom` à l'indice `0`) des animaux volants (attribut `volant` à l'indice `5`):

```pycon
>>> [animal[0] for animal in animaux if animal[5]]
['moustique', 'frelon', 'papillon', 'blatte', 'libellule', 'cigale', 'fourmi']
```

!!! note "Remarque"

    Pour les attributs de type booléen, il est inutile de tester l'égalité de la valeur à `#!py True` ou `#!py False`. Par exemple `if animal[5]` sélectionne directement les animaux volants, `#!py if not animal[5]` ceux qui ne volent pas.

Il est aussi possible de compter les résultats en faisant par exemple (nombre d'animaux aquatiques sans pattes) :

```pycon
>>> len([animal for animal in animaux if animal[6] and animal[13] == 0])
10
```

Compléter le code ci-dessous en effectuant les requêtes proposées.

{{ IDE('exo') }}
