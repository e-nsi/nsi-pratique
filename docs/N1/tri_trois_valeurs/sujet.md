---
author: Nicolas Revéret
title: Tri de trois valeurs
tags:
    - 1-boucle
status: relecture
---

# Tri de trois valeurs

!!! warning "Attention"
    On interdit dans cet exercice d'utiliser les fonctions de tri de Python.

On souhaite trier un tableau `nombres` non-vide ne contenant que des valeurs `0`, `1` ou `2` en quantité quelconque. Par exemple `nombres = [2, 0, 2, 1, 2, 1]`.

Dans ce cas de figure, on peut utiliser une méthode efficace qui ne lit qu'une seule fois chaque valeur et modifie directement le tableau.

L'idée est de maintenir quatre zones dans le tableau :

* la première zone ne contient que des `0` (en bleu ci-dessous),
* la deuxième que des `1` (en blanc),
* la troisième contient les nombres pas encore triés (en gris),
* la dernière zone ne contient que des `2` (en rouge).

![Étape intermédiaire](figures/bis_etape_3.svg){width="30%"}

Ces zones sont délimitées par les bornes suivantes (incluses) :

1. de l'indice `0` à `i0 - 1`,
2. de `i0` à `i1 - 1`,
3. de `i1` à `i2`,
4. de `i2 + 1` à `#!py len(nombres) - 1`

L'algorithme à coder est donc le suivant

* on crée une variable `i0` égale à `0`,
* on crée une variable `i1` égale à `0`,
* on crée une variable `i2` égale au dernier indice de `nombres`,
* on répète les étapes ci-dessous tant que `i1` est inférieur ou égal à `i2` :
    * si la valeur d'indice `i1` est un `0`, on l'échange avec celle d'indice `i0` et l'on incrémente `i0` et `i1`,
    * si c'est un `1`, on incrémente `i1`,
    * sinon c'est un `2` : on l'échange avec celle d'indice `i2` et l'on décrémente `i2`.

=== "Étape 0"

    ![Étape 0](figures/bis_etape_0.svg){width="30%"}

    * `i0 = 0`
    * `i1 = 0`
    * `i2 = 5`
  
=== "Étape 1"

    ![Étape 1](figures/bis_etape_1.svg){width="30%"}

    * `i0 = 0`
    * `i1 = 0`
    * `i2 = 4`

=== "Étape 2"

    ![Étape 2](figures/bis_etape_2.svg){width="30%"}

    * `i0 = 0`
    * `i1 = 1`
    * `i2 = 4`

=== "Étape 3"

    ![Étape 3](figures/bis_etape_3.svg){width="30%"}

    * `i0 = 1`
    * `i1 = 2`
    * `i2 = 4`

=== "Étape 4"

    ![Étape 4](figures/bis_etape_4.svg){width="30%"}

    * `i0 = 1`
    * `i1 = 2`
    * `i2 = 3`

=== "Étape 5"

    ![Étape 5](figures/bis_etape_5.svg){width="30%"}

    * `i0 = 1`
    * `i1 = 2`
    * `i2 = 2`

=== "Étape 6"

    ![Étape 6](figures/bis_etape_6.svg){width="30%"}

    * `i0 = 1`
    * `i1 = 3`
    * `i2 = 2`

!!! tip "Échange de valeurs"

    Il est possible d'échanger deux valeurs d'indices `i` et `j` en faisant `nombres[i], nombres[j] = nombres[j], nombres[i]`.

!!! example "Exemples"

    ```pycon
    >>> tri_3_valeurs([2, 0, 2, 1, 2, 1])
    [0, 1, 1, 2, 2, 2]
    >>> tri_3_valeurs([1, 1, 0, 1])
    [0, 1, 1, 1]
    ```

{{ IDE('exo', SANS = 'sort, sorted') }}
