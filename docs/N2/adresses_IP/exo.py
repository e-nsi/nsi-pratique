class AdresseIP:
    """Définit une adresse IPV4 associée à son masque de sous-réseau"""
    
    def __init__(self, adresse, masque):
        self.adresse = ...
        self.masque = ...

    def nombre_magique(self):
        """renvoie l'indice de l'octet significatif et le nombre magique"""
        i = 3
        while self.masque[i] == 0:
            i -= 1
        octet_significatif = self.masque[i]
        return i, 256 - octet_significatif

    def plage(self):
        """Calcule la première et la dernière adresse et renvoie
        le couple constitué de ces 2 listes
        """
        indice, magic = self.nombre_magique()
        multiple = (self.adresse[indice] // magic) * magic
        
        adr_debut = [..., ..., ..., ...]
        for i in range(indice):
            adr_debut[i] = ...
        adr_debut[indice] = ...
        
        adr_fin = [..., ..., ..., ...]
        for i in range(indice):
            adr_fin[i] = ...
        adr_fin[indice] = ...
        return adr_debut, adr_fin

# tests

adresse_enonce = AdresseIP('192.168.0.1', '255.224.0.0') 
assert adresse_enonce.plage_adresses() == ([192, 160, 0, 0], [192, 191, 255, 255])
