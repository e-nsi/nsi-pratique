class AdresseIP:
    """Définit une adresse IPV4 associée à son masque de sous-réseau"""

    def __init__(self, adresse, masque):
        self.adresse = [int(i) for i in adresse.split('.')]
        self.masque = [int(i) for i in masque.split('.')]

    def nombre_magique(self):
        """renvoie l'indice de l'octet significatif et le nombre magique"""
        i = 3
        while self.masque[i] == 0:
            i -= 1
        octet_significatif = self.masque[i]
        return i, 256 - octet_significatif

    def plage_adresses(self):
        indice, magic = self.nombre_magique()
        debut_octet_significatif = (self.adresse[indice] // magic) * magic
        adr_debut = [0, 0, 0, 0]
        for i in range(indice):
            adr_debut[i] = self.adresse[i]
        adr_debut[indice] = debut_octet_significatif
        
        adr_fin = [255, 255, 255, 255]
        for i in range(indice):
            adr_fin[i] = self.adresse[i]
        adr_fin[indice] = debut_octet_significatif + magic - 1
        return adr_debut, adr_fin
        
    