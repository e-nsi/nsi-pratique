# tests

adresse_enonce = AdresseIP('192.168.0.1', '255.224.0.0')
assert adresse_enonce.plage_adresses() == ([192, 160, 0, 0], [192, 191, 255, 255])

# Autres tests

adr_1 = AdresseIP('10.45.185.24', '255.255.248.0')
assert adr_1.plage_adresses() == ([10, 45, 184, 0], [10, 45, 191, 255])

adr_2 = AdresseIP('172.16.1.220', '255.255.0.0')
assert adr_2.plage_adresses() == ([172, 16, 0, 0], [172, 16, 255, 255])

adr_3 = AdresseIP('192.154.88.133', '255.255.255.192')
assert adr_3.plage_adresses() == ([192, 154, 88, 128], [192, 154, 88, 191])

adr_4 = AdresseIP('131.108.78.235', '255.255.248.0')
assert adr_4.plage_adresses() == ([131, 108, 72, 0], [131, 108, 79, 255])
