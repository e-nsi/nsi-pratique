---
author: Fabrice NATIVEL
title: Texte brut issu d'un document HTML
---

# Extraction du texte brut d'un document html

On dispose d'une chaîne de caractères représentant une portion de code HTML, par exemple `<p> un <strong>exemple</strong> de chaine</p>` et on souhaite écrire une fonction `extrait_texte_brut` qui supprime toutes balises HTML de cette chaîne et renvoie le texte brut qu'il contient. Par exemple : `extrait_texte_brut("<p>un <strong>exemple</strong> de chaine</p>")` doit renvoyer `"un exemple de chaine"`.

On suppose qu'un caractère `<` déclarant une ouverture de balise est *toujours* suivi plus loin dans la chaîne d'un caractère `>`. D'autre part, il n'y a **jamais** de `<` imbriquées.

{{ IDE('exo') }}


---

TODO, je (FC) modifie le sujet pour proposer que brut aille en niveau 1, il y a une façon simple de faire

```python
def texte_brut(source_html):
    """
    Renvoie source_html sans les balises
    
    >>> texte_brut("<p>Le <strong>join</strong>, c'est fantastique.</p>")
    "Le join, c'est fantastique."
    """
    sortie = ""
    balise = False
    for c in source_html:
        if c == '<':
            balise = True
        elif c == '>':
            balise = False
        else:
            if not balise:
                sortie += c
    return sortie


import doctest
doctest.testmod()
```
