assert additionneur(0, 0, 0) == (0, 0)
assert additionneur(1, 0, 0) == (1, 0)
assert additionneur(0, 1, 0) == (1, 0)
assert additionneur(0, 0, 1) == (1, 0)
assert additionneur(1, 1, 0) == (0, 1)
assert additionneur(0, 1, 1) == (0, 1)
assert additionneur(1, 0, 1) == (0, 1)
assert additionneur(1, 1, 1) == (1, 1)

assert addition_binaire(
  [0, 0, 0, 0, 1, 0, 1, 0], [0, 0, 0, 0, 0, 1, 0, 1]) == [0, 0, 0, 0, 1, 1, 1, 1]
assert addition_binaire(
  [0, 1, 1, 1, 1, 1, 1, 1], [0, 0, 0, 0, 0, 0, 0, 1]) == [1, 0, 0, 0, 0, 0, 0, 0]
