---
author: Nicolas Revéret
title: Compression RLE
tags:
    - string
---

# Compression *Run-Length Encoding*

Certains formats de fichiers informatiques utilisent la compression *Run-Length Encoding*.

Le principe est de remplacer les suites de valeurs identiques par des couples `(valeur, répétition)`.

On propose ci-dessous un exemple d'application à un fichier texte :

!!! example "Exemple"

    ```pycon
    >>> compression_RLE("aabbbbcaa")
    "2a4b1c2a"
    ```

Le code `"2a4b1c2a"` signifie que la chaîne `"aabbbbcaa"` comporte dans cet ordre :

* deux `"a"`,
* quatre `"b"`,
* un `"c"`,
* deux `"a"`.

!!! note "Remarque"

    Cette méthode de compression est particulièrement efficace dans le cas d'un texte comportant de nombreuses répétitions.
    
    Par exemple, le texte `'aaaa...a'` comportant 10 000 fois le caractère `"a"` sera compressé en `"10000a"`. On passe ainsi de 10 000 caractères à 6 !

Afin de simplifier la démarche, on ne considérera que des chaînes de caractères ne comportant aucun chiffre. Ainsi, toutes les chaînes étudiées ne comporteront que des lettres ou de la ponctuation.

Afin de calculer la compression d'une chaîne de caractères par cette méthode, on doit la parcourir en comptant le nombre de répétitions de chaque caractère lu. Lorsque l'on change de caractère, on ajoute le couple `(répétition, caractères)` à la chaîne compressée, sans oublié d'ajouter une virgule si besoin.

!!! example "Exemples"

    ```pycon
    >>> compression_RLE("aabbbbcaa")
    "2a4b1c2a"
    >>> compression_RLE("aa aa")
    "2a1 2a"
    >>> compression_RLE("aaa")
    "3a"
    >>> compression_RLE("aA")
    "1a1A"
    ```

Vous devez écrire la fonction `compression(texte)` décrite.

On garantit que le texte proposé est non-vide.

{{ py_sujet('exo')}}


???+ tip "Coup de pouce"

    Python ne supporte pas "l'addition" d'un caractère et d'un entier. On pourra utiliser la méthode suivante :

    ```pycon
    >>> str(5) + "a"
    "a5"
    ```

{{ IDE('exo') }}
