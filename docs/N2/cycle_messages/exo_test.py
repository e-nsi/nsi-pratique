# tests

assert est_cyclique({'A': 'E', 'F': 'A', 'C': 'D', 'E': 'B', 'B': 'F', 'D': 'C'}) == False
assert est_cyclique({'A': 'E', 'F': 'C', 'C': 'D', 'E': 'B', 'B': 'F', 'D': 'A'}) == True
assert est_cyclique({'A': 'B', 'F': 'C', 'C': 'D', 'E': 'A', 'B': 'F', 'D': 'E'}) == True
assert est_cyclique({'A': 'B', 'F': 'A', 'C': 'D', 'E': 'C', 'B': 'F', 'D': 'E'}) == False

# autres tests

assert est_cyclique({}) == True
assert est_cyclique({'A': 'A', 'B': 'B'}) == False
assert est_cyclique({'A': 'B', 'B': 'A'}) == True
assert est_cyclique({'A': 'B', 'B': 'C', 'C': 'D', 'D': 'A'}) == True
assert est_cyclique({'A': 'B', 'B': 'C', 'C': 'D', 'D': 'A', 'E': 'E'}) == False
