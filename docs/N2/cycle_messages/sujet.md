---
author: BNS2022-14.2, puis Romain JANVIER
title: Cycles
tags:
  - dictionnaire
  - boucle
---

# Recherche d'un cycle dans une permutation

On considère des personnes, identifiées par un pseudo unique, qui s'envoient des messages en respectant les deux règles suivantes :

- chaque personne ne peut envoyer des messages qu'à 1 seule et même personne
(éventuellement elle-même),
- chaque personne ne peut recevoir des messages qu'en provenance d'une seule
personne (éventuellement elle-même).

Voici un exemple, avec 6 personnes, de « plan d'envoi des messages » qui respecte les
règles ci-dessus, puisque *chaque personne est présente une seule fois dans chaque
colonne* :

- Anne envoie ses messages à Elodie
- Elodie envoie ses messages à Bruno
- Bruno envoie ses messages à Fidel
- Fidel envoie ses messages à Anne
- Claude envoie ses messages à Denis
- Denis envoie ses messages à Claude

Et le dictionnaire correspondant à ce plan d'envoi est le suivant :

```python
plan_a = {'Anne': 'Elodie', 'Bruno': 'Fidel', 'Claude': 'Denis', 
          'Denis': 'Claude', 'Elodie': 'Bruno', 'Fidel': 'Anne'}
```

Sur le plan d'envoi `plan_a` des messages ci-dessus, il y a deux cycles distincts : un premier
cycle avec Anne, Elodie, Bruno, Fidel et un second cycle avec Claude et Denis. En revanche, le plan d'envoi `plan_b` ci-dessous :

```python
plan_b = {'Anne': 'Claude', 'Bruno': 'Fidel', 'Claude': 'Elodie', 
          'Denis': 'Anne', 'Elodie': 'Bruno', 'Fidel': 'Denis'}
```

comporte un unique cycle : Anne, Claude, Elodie, Bruno, Fidel, Denis. Dans ce cas, lorsqu'un plan d'envoi comporte un unique cycle, on dit que le plan d'envoi est *cyclique*.

!!! info "Conséquence des règles sur le dictionnaire"
    Les deux règles garantissent que chaque personne apparaît une fois en
    tant que clé du dictionnaire et exactement une fois en tant que valeur
    associée à une clé.

Pour savoir si un plan d'envoi de messages comportant $\text{N}$ personnes est cyclique, on peut utiliser l'algorithme ci-dessous :


On part d'une personne A et on inspecte les $\text{N}-1$ successeurs dans le plan d'envoi :

- Si un de ces $\text{N}-1$ successeurs est A lui-même, on a trouvé un cycle de taille
inférieure ou égale à $\text{N}-1$. Il y a donc au moins deux cycles et le plan d'envoi n'est
pas cyclique.

- Si on ne retombe pas sur A lors de cette inspection, on a un unique cycle qui passe
par toutes les personnes : le plan d'envoi est cyclique. En effet, le
dernier doit forcément écrire à A.


Compléter la fonction suivante en respectant la spécification.

??? tip "Longueur d'un dictionnaire"
    La fonction python `len` permet d'obtenir la longueur d'un dictionnaire.



!!! example "Exemples"

    ```pycon
    >>> est_cyclique({'A': 'E', 'F': 'A', 'C': 'D', 'E': 'B', 'B': 'F', 'D': 'C'})
    False
    >>> est_cyclique({'A': 'E', 'F': 'C', 'C': 'D', 'E': 'B', 'B': 'F', 'D': 'A'})
    True
    >>> est_cyclique({'A': 'B', 'F': 'C', 'C': 'D', 'E': 'A', 'B': 'F', 'D': 'E'})
    True
    >>> est_cyclique({'A': 'B', 'F': 'A', 'C': 'D', 'E': 'C', 'B': 'F', 'D': 'E'})
    False
    ```

{{ IDE('exo') }}



