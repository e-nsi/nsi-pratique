---
author: BNS2022-15.2, puis Romain Janvier
title: Conversion en binaire
tags:
  - binaire
  - boucle
---

# Conversion d'entiers en base 10 vers la base 2

Pour rappel, la conversion d'un nombre entier positif en binaire peut s'effectuer à l'aide des divisions successives comme illustré ici :

![image](img15_2b.png){: .centrer width=40% }

Compléter la fonction `binaire` basée sur la méthode des divisions successives permettant de
convertir un nombre entier positif en binaire.

Le tableau `BITS` permet d'obtenir la chaîne de caractères
correspondant à un bit à partir de sa valeur.

!!! example "Exemples"

    ```pycon
    >>> BITS[0]
    '0'
    >>> BITS[1]
    '1'
    ```
    
    ```pycon
    >>> binaire(0)
    '0'
    >>> binaire(1)
    '1'
    >>> binaire(16)
    '10000'
    >>> binaire(77)
    '1001101'
    ```

{{ IDE('exo') }}
