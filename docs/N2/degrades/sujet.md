---
author: Sébastien HOARAU
title: Dégradés
tags:
    - boucle
    - tuple
description: Créer une liste de tuple en se servant des résultats d'appels à une fonction intermédiaire fournie. 
difficulty: Exercice guidé
---

# Un générateur de dégradés de couleurs

Bob est _webdesigner_ et triste. Il a l'habitude d'utiliser un outil en ligne pour créer de beaux dégradés de couleurs pour ses sites web :

![image blender color](blender.png)

 Mais aujourd'hui le site est hors-ligne. Nous allons aider Bob en écrivant une fonction qui génère le dégradé. 
 
 Les couleurs sont codées par un triplet $(r, v, b)$ qui représente dans l'ordre la quantité de rouge, de vert et de bleu. Chaque composante est un entier entre $0$ et $255$. 

L'outil de Bob prend trois paramètres :

- une couleur de départ `couleur_1`
- une couleur d'arrivée `couleur_2`
- un nombre `nb` de points intermédiaires

Le résultat est une liste de couleurs : `couleur_1`, `intermediaire_1`, ..., `intermediaire_nb`, `couleur_2`.  

## Calcul des couleurs intermédiaires

Voilà, sur un exemple, comment calculer les couleurs intermédiaires, c'est-à-dire les valeurs intermédiaires de chacune des composantes. On se donne deux couleurs :


```pycon
>>> couleur_1 = (100, 200, 50)
>>> couleur_2 = (150, 100, 100)
```

Supposons qu'on souhaite $3$ couleurs intermédiaires. Pour la première composante, on doit passer de $100$ à $150$ sur $4$ intervalles. Il faut donc répartir $50$ points en $4$, par exemple $13$, $13$, $12$, $12$. 

La première composante de la couleur `intermediaire_1` vaudra $113$. Celle de la couleur `intermediaire_2` vaudra $126$. Ainsi de suite et on procède de même pour les deux autres composantes.

## Le calcul du dégradé

Nous vous fournissons une fonction `repartir` qui prend deux entiers en paramètres : `a_diviser` qui est la quantité à diviser équitablement (dans notre exemple $100$) et `nb_parts` en combien on doit couper la valeur `a_diviser` ($4$ dans l'exemple).

Il s'agira pour vous de compléter la fonction `degrade` qui prend les mêmes trois paramètres dont l'outil de Bob a besoin et renvoie la liste des couleurs formant le dégradé comme expliqué ci-dessus.

!!! note "Indication"

    Lorsqu'on a un triple `mon_triplet`, on peut utiliser le _décompactage_ pour récupérer les 3 valeurs dans des variables, comme ceci :

    ```python
    a, b, c = mon_triplet
    ```


## Exemples

!!! example "Exemples"

      ```pycon
      >>> C1 = (150, 80, 120)
      >>> C2 = (230, 20, 160)
      >>> degrade(C1, C2, 3)
      [(150, 80, 120),
       (170, 65, 130),
       (190, 50, 140),
       (210, 35, 150),
       (230, 20, 160)]
      ```

      Dans cet exemple tous les intervalles de valeurs étaient divisibles par $4$, donnant un dégradé parfaitement uniforme. Ce n'est pas forcément le cas, ce n'est pas grave : certains intervalles seront un peu plus grand que les autres c'est tout (un exemple où on divise par $5$) :

      ```pycon
      >>> C3 = (10, 100, 220)
      >>> C4 = (102, 200, 178)
      >>> degrade(C3, C4, 4)
      [(10, 100, 220),
       (29, 120, 212),
       (48, 140, 204),
       (66, 160, 196),
       (84, 180, 187),
       (102, 200, 178)]
      ```

      Ici les deux premiers intervalles sont plus grands que les trois autres (de $1$).


## À vous de jouer

Dans l'IDE suivant, compléter la fonction et tenter de faire valider votre programme.

{{ IDE('exo') }}
