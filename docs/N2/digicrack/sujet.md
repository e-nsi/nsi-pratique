---
author: Jean Diraison
title: Casseur de Digicode
tags:
  - maths
---

# Casseur de Digicode

Vous devez réaliser une fonction `digicrack` permettant de casser n'importe quel digicode à 4 chiffres (de 0 à 9 inclus).

Votre fonction doit s'adapter à n'importe quel digicode, c'est pour cela qu'elle prend en paramètre une fonction `digicode` qui possède 4 paramètres `a`, `b`, `c` et `a` correspondant chacun à un chiffre en base 10 et qui renvoie `True` si la combinaison formée par ces 4 chiffres est valide et `False` sinon. C'est cette fonction qui autorise l'ouverture d'une porte à combinaison.

La fonction `digicrack` doit renvoyer l'un des tuples `(a, b, c, d)` correspondant à une des combinaisons valides.

Ainsi à titre d'exemple, pour la fonction `digicode1` qui suit :
    ```pycon
    >>> def digicode1(a, b, c, d):
    ...     return not bool((((a ^ b ^ 6) << ((c & 7) + d)) + ((a + c) ** b + d * 17 + 2022)) % 9973)
    ```
la fonction `digicode` appelée avec l'argument `digicode1` peut renvoyer le tuple `(2, 8, 7, 4)` puisque ces 4 chiffres constituent une combinaison valide pour `digicode1` (l'appel `digicode1(2, 8, 7, 4)` renvoie `True`).


!!! Exemple

    ```pycon
    >>> def digicode1(a, b, c, d):
    ...     return not bool((((a ^ b ^ 6) << ((c & 7) + d)) + ((a + c) ** b + d * 17 + 2022)) % 9973)
    ...
    >>> digicrack(digicode1)
    (2, 8, 7, 4)
    ```

{{ IDE('exo') }}
