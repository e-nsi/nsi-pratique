## Commentaires

{{ IDE('exo_corr') }}

Cet exercice permet de se familiariser avec les méthodes spéciales de Python. Outre celles mentionnées dans le sujet, on rencontre entre autres :

* `__ne__` : détermine si deux objets sont différents (*not equal*),
* `__gt__` : détermine si l'objet qui subit la méthode est strictement supérieur à celui passé en argument (*greater than*),
* `__ge__` : détermine si l'objet qui subit la méthode est supérieur ou égal à celui passé en argument (*greater or equal*),
* `__lt__` : détermine si l'objet qui subit la méthode est strictement inférieur à celui passé en argument (*less than*),
* `__le__` : détermine si l'objet qui subit la méthode est inférieur ou égal à celui passé en argument (*less or equal*),
* `__mul__` : multiplie l'objet qui subit la méthode et celui passé en argument. Renvoie un nouvel objet.

Certaines des méthodes spéciales de Python sont définies par défaut lorsque l'on crée une classe :

```pycon
>>> class Chose:
...     def __init__(self):
...         self.a = 8
...
>>> truc = Chose()
>>> truc
<__main__.Chose object at 0x000001582287BEE0>
>>> truc.__repr__()
'<__main__.Chose object at 0x000001582287BEE0>'
```

Dans l'exemple précédent on crée une classe `Chose` minimaliste dans laquelle chaque objet possède un attribut `a` toujours égal à `8`.

Comme on peut le voir, saisir `truc` dans la console renvoie, aux guillemets près, le même résultat que l'appel `truc.__repr__()`. En effet, lorsqu'il doit représenter `truc`, Python exécute l'appel `print(truc.__repr__())` : la méthode `__repr__(self)` a été définie par défaut.