---
author: BNS2022-23.2, puis Nicolas Revéret
title: Évaluation postfixe
tags:
  - pile
---

# Évaluation d'une expression postfixe

Nous avons l'habitude de noter les expressions arithmétiques avec des parenthèses comme : $(2 + 3) \times 5$. 

Il existe une autre notation utilisée par certaines calculatrices, appelée notation postfixe, qui n'utilise pas de parenthèses. L'expression arithmétique précédente est alors obtenue en saisissant successivement $2$, puis $3$, puis l'opérateur $+$, puis $5$, et enfin l'opérateur $\times$.

On modélise cette saisie par la liste Python `[2, 3, '+', 5, '*']`. 

De la même façon, la notation postfixe de $3 \times 2 + 5$ est modélisée par la liste `[3, 2, '*', 5, '+']`. 

L'évaluation (le calcul) d'une expression arithmétique en notation postfixe peut s'obtenir à l'aide d'une pile en parcourant l'expression arithmétique de gauche à droite de la façon suivante :

* Si l'élément lu est un nombre, on le place au sommet de la pile.
* Si c'est un opérateur, on récupère les deux valeurs situées au sommet de la pile et on leur applique l'opération. On place le résultat au sommet de la pile.
* À la fin du parcours, il reste un seul élément dans la pile : c'est le résultat de l'expression arithmétique.

Dans le cadre de cet exercice, on se limitera aux opérations $\times$ et $+$. On garantit de plus que l'expression est "bien formée", c'est-à-dire que l'expression arithmétique a du sens ($3 \times 2$ a du sens, pas $3\;+\;\times$).

Pour cet exercice, on dispose d'une classe `Pile` qui implémente les méthodes de base sur la structure de pile.

Compléter le script de la fonction `evaluation_postfixe` qui :

* prend en paramètre une liste Python représentant la notation postfixe d'une expression arithmétique,

* renvoie sa valeur associée.

!!! example "Exemples"

    ```pycon
    >>> evaluation_postfixe([3, 2, '*', 5, '+']) # correspond à 3 * 2 + 5
    11
    >>> evaluation_postfixe([2, 3, '+', 5, '*']) # correspond à (2 + 3) * 5
    25
    >>> evaluation_postfixe([2]) # correspond à 2
    2
    >>> evaluation_postfixe([2, 3, 4, '*', '*']) # correspond à 4 * 3 * 2
    24
    ```

{{ IDE('exo') }}
