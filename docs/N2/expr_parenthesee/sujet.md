---
author: Franck Chambon
title: Expression parenthésée
tags:
  - 6-recursivité
  - 7-POO
  - 8-arbre
---

# Expression parenthésée

Une expression arithmétique ne comportant que les quatre opérations $+, - , ×, ÷$ peut être représentée sous forme d'arbre. La disposition des nœuds indique les priorités opératoires.

- **Les feuilles** représentent des nombres, elles ne possèdent pas de sous-arbre.
- **Les nœuds internes** représentent des opérateurs binaires, ils possèdent exactement deux sous arbres.

Par exemple, avec un parcours en profondeur infixe de l'arbre ci-dessous, on
peut retrouver l'expression notée habituellement : $((3 × (8 + 7)) - (2 + 1))$.

![arbre](arbre.svg){ .autolight }


La classe `Noeud` ci-après met en œuvre une structure d'arbre adaptée.

!!! warning "Il n'agit pas exactement d'un arbre binaire"
    Avec un arbre binaire, tous les nœuds ont exactement deux sous-arbres. Un sous arbre peut alors être vide.

    Ici, il s'agit d'un arbre d'expression pour opérateurs binaires. C'est un arbre d'arité 2 ; il n'y a pas, ici, de notion d'arbre vide. Ici, chaque nœud possède 0 ou 2 sous-arbre.

Compléter la fonction récursive `expression_parenthesee` qui prend en paramètre un objet de la classe `Noeud` qui désigne la racine d'un arbre et qui renvoie l'expression arithmétique représentée par l'arbre passé en paramètre, sous forme d'une chaine de caractères contenant des parenthèses.

!!! example "Exemple"
    Résultat attendu avec l'arbre ci-dessus :

    ```pycon
    >>> somme_1 = Noeud(Noeud(None, 8, None), '+', Noeud(None, 7, None))
    >>> somme_2 = Noeud(Noeud(None, 2, None), '+', Noeud(None, 1, None))
    >>> produit_1 = Noeud(Noeud(None, 3, None), '*', somme_1)
    >>> expression = Noeud(produit_1, '-', somme_2)
    >>> expression_parenthesee(expression)
    '((3*(8+7))-(2+1))'
    ```

    D'autres exemples

    ```pycon
    >>> feuille = Noeud(None, 5, None)
    >>> expression_parenthesee(feuille)
    '5'
    ```

    ```pycon
    >>> somme = Noeud(Noeud(None, 2, None), '+', Noeud(None, 3, None))
    >>> expression_parenthesee(somme)
    '(2+3)'
    ```

    ```pycon
    >>> somme = Noeud(Noeud(None, 2, None), '+', Noeud(None, 3, None))
    >>> produit = Noeud(Noeud(None, 4, None), '*', somme)
    >>> expression_parenthesee(produit)
    '(4*(2+3))'
    ```

{{ IDE('exo') }}
