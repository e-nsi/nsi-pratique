# Imports

# Le hasard
import random

# Les maths
import math

# Les graphes
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import seaborn as sns


def basePerlin(w: int, h: int, largeur: int) -> list:
    """
    Renvoie la grille utilisée dans le bruit de Perlin
    A chaque coin de la grille (tous les largeur pixels) on crée un vecteur unitaire en 2D
    """
    valeurs = dict()

    for y in range(0, h, largeur):
        for x in range(0, w, largeur):
            alpha = random.random()*2*math.pi
            valeurs[x, y] = (math.cos(alpha), math.sin(alpha))

    return valeurs


def produitScalaire(x1: float, y1: float, x2: float, y2: float) -> float:
    """
    Calcule le produit scalaire en utilisant les coordonnées fournies
    """
    return x1*x2 + y1*y2


def amortissement(t: float) -> float:
    """
    Fonction calculant un amortissement.
    Voir https://www.cs.umd.edu/class/spring2018/cmsc425/Lects/lect13-2d-perlin.pdf
    """
    return t*t*t*(t*(t*6-15)+10)  # ->6*t**5-15*t**4+10*t**3


def perlin(w: int, h: int, largeur: int, mini: int, maxi: int) -> list:
    """"
    Crée une grille de nombres aléatoires en utilisant le bruit de Perlin
    """
    coins = basePerlin(w+1, h+1, largeur)
    grille = [[0 for x in range(w)] for y in range(h)]

    gMini = float('inf')
    gMaxi = -float('inf')

    for y in range(h):
        for x in range(w):
            xGauche = (x//largeur)*largeur
            yBas = (y//largeur)*largeur
            xDroit = xGauche+largeur
            yHaut = yBas+largeur

            dx = (x - xGauche)/largeur
            dy = (y - yBas)/largeur
            amortissementX = amortissement(dx)
            amortissementY = amortissement(dy)

            bruitX = (1-amortissementX) * \
                produitScalaire(*coins[xGauche, yBas], dx, dy)
            bruitX += (amortissementX)*produitScalaire(*
                                                       coins[xDroit, yBas], dx-1, dy)

            bruitY = (1-amortissementX)*produitScalaire(*
                                                        coins[xGauche, yHaut], dx, dy-1)
            bruitY += (amortissementX)*produitScalaire(*
                                                       coins[xDroit, yHaut], dx-1, dy-1)

            grille[y][x] = (1-amortissementY)*bruitX + (amortissementY)*bruitY
            if grille[y][x] < gMini:
                gMini = grille[y][x]
            elif grille[y][x] > gMaxi:
                gMaxi = grille[y][x]

    # Mise à l'échelle
    a = (mini-maxi)/(gMini-gMaxi)
    b = mini-a*gMini

    for y in range(h):
        for x in range(w):
            grille[x][y] = a*grille[x][y]+b
    return grille


def perlinHarmonique(iterations: int, w: int, h: int, mini: int, maxi: int) -> list:
    """
    Effectue plusieurs itérations du bruit de Perlin
    """
    while w // 2**iterations == 0:
        iterations -= 1

    grillesBruits = []
    for k in range(iterations):
        grillesBruits.append(perlin(w,
                                    h,
                                    int(w//2**(k+1)),
                                    mini/2**k,
                                    maxi/2**k))

    grilleBruit = []
    for y in range(h):
        grilleBruit.append([])
        for x in range(w):
            s = 0
            for k in range(iterations):
                s += grillesBruits[k][y][x]
            grilleBruit[-1].append(int(s))  # Conversion en entier

    return grilleBruit


nom_fichiers = "grille64"

puissance = 6
w, h = 2**puissance, 2**puissance
mini = 0
maxi = 50
bruit = perlinHarmonique(3, w, h, mini, maxi)

# Ajout des bords
for ligne in bruit:
    ligne.insert(0, 2*maxi)
    ligne.append(2*maxi)
bruit.insert(0, [2*maxi for _ in range(w+2)])
bruit.append([2*maxi for _ in range(w+2)])

# Dessin
sns.heatmap(bruit, linewidths=0.5, cmap='coolwarm')
plt.tight_layout()
# plt.show()
plt.savefig(f"{nom_fichiers}.png")

# Sauvegarde de la grille
with open(f"{nom_fichiers}.txt", "w") as f:
    f.write(str(bruit))
