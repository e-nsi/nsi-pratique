---
author: Nicolas Revéret
title: Golf
tags:
    - glouton
status: relecture
---

# Jouons au golf !

On cherche dans cet exercice à simuler le déplacement d'une balle de golf roulant sur le *green*.

Le terrain sera représenté par une liste Python dont chaque élément est une liste de nombres entiers positifs. Les nombres contenus dans les sous-listes sont les hauteurs des différents points du terrain.

On donne un exemple ci-dessous (en bleu foncé les points de faible hauteur, en rouge foncé ceux de grande hauteur).

![terrain en 2D](images/grille8.png){ width="75%" }

La liste Python correspondant à ce terrain est donnée ci-dessous :

```python
terrain = [
    [100, 100, 100, 100, 100, 100, 100, 100, 100, 100],
    [100,  39,  17,  16,  30,  39,  56,  62,  51, 100],
    [100,  39,  18,   4,  10,  20,  43,  62,  53, 100],
    [100,  27,  22,  12,  18,  34,  43,  46,  42, 100],
    [100,  15,  23,  19,  30,  49,  45,  38,  30, 100],
    [100,  39,  43,  25,  27,  39,  36,  40,  40, 100],
    [100,  64,  59,  38,  36,  43,  39,  31,  31, 100],
    [100,  60,  49,  38,  42,  47,  55,  40,  22, 100],
    [100,  51,  38,  33,  45,  43,  49,  49,  28, 100],
    [100, 100, 100, 100, 100, 100, 100, 100, 100, 100]
]
```

On peut vérifier dans la grille que le point de la ligne `2` et de la colonne `3` est le plus bas du terrain : `terrain[2][3]` vaut `4` qui est la valeur minimale.

On garantit que toutes les sous-listes ont la même dimension. On précise de plus que les points des quatre bords ont tous des hauteurs strictement supérieures aux points de l'intérieur du terrain.

La balle se trouve initialement sur la ligne `i` et la colonne `j`. La hauteur du point correspondant est donc `terrain[i][j]`.

Depuis cette position initiale, la balle va "rouler" vers l'une des cases adjacentes dont la hauteur est **strictement inférieure** à celle de sa position actuelle.

Elle peut se déplacer dans 8 directions :

* ↖ en haut à gauche,
* ↑ en haut,
* ↗ en haut à droite,
* ← à gauche,
* → à droite,
* ↙ en bas à gauche,
* ↓ en bas,
* ↘ en bas à droite.

Ces déplacements sont déclarés dans la liste Python ci-dessous :

```python
deplacements_possibles = [(-1, -1),
                          ( 0, -1),
                          (+1, -1),
                          (-1,  0),
                          (+1,  0),
                          (-1, +1),
                          ( 0, +1),
                          (+1, +1)
]
```

Dans le cas où plusieurs cases adjacentes ont des hauteurs inférieures à la case de la balle, celle-ci ira vers la case dont la hauteur est minimale (ce déplacement entraîne donc une variation maximale).

La bille roule jusqu'à arriver sur un point de hauteur minimale : tous les points voisins ont une hauteur **supérieure ou égale** à la sienne.

L'algorithme permettant de déterminer la position finale peut être construit ainsi :

* on parcourt l'ensemble des déplacements possibles afin de déterminer celui qui entraîne la variation de hauteur maximale. Lors de la recherche, la valeur du maximum est initialisée à `0` (pas de déplacement),
* dans le cas où deux déplacements entraînent la même variation, on convient de **conserver le premier rencontré** dans la liste des déplacements possibles,
* ce déplacement "maximal" trouvé, on l'applique à la balle.

{{ py('exo', 0, '# TESTS') }}

!!! tip "Coup de pouce"

    On pourra facilement parcourir ces déplacements et récupérer les déplacements horizontaux et verticaux correspondants en faisant :

    ```python
    for depl_i, depl_j in deplacements_possibles:
        # Action
    ```

Écrire la fonction `rouler` :

* prenant en arguments le `terrain` (liste de listes) et la position de départ (numéro de ligne `i` et numéro de colonne `j`),

* et renvoyant la position finale de la balle sous forme d'un tuple.

{{ IDE('exo') }}
