# Commentaires

{{ IDE('exo_corr') }}

Prenons un exemple : afin de calculer la somme des chiffres de $409$ on doit additionner le chiffre des unités $9$, celui des dizaines $0$ et celui des centaines $4$.

Le chiffre des unités vaut $9$. C'est le reste de la division de $409$ par $10$. On l'obtient en Python en faisant `409 % 10`.

Ce calcul effectué, on calcule le quotient de $409$ par $10$ : on obtient $40$. On l'obtient en Python en faisant `409 // 10`.

On peut alors recommencer la démarche (reste de la division suivie de quotient) jusqu'à ce que le quotient soit nul.

| Etape | Valeur de $n$ | Reste de la division par $10$ | Quotient de la division par $10$ |
| :---: | :-----------: | :---------------------------: | :------------------------------: |
|   1   |     $409$     |              $9$              |               $40$               |
|   2   |     $40$      |              $0$              |               $40$               |
|   3   |      $4$      |              $4$              |               $0$                |

La fonction `somme_chiffres` est donc construite autour d'une boucle non-bornée (`while`) se répétant tant que `n` est non nul.

A chaque itération on ajoute le reste à la variable `somme` et l'on remplace la valeur de `n` par son quotient par `10`.

On renvoie la `somme`.

La fonction `harshad` est simple : on se contente de tester si le reste de la division de `n` par `somme_chiffres(n)` est égal à `0`. Si oui, le nombre est *harshad*, si non il ne l'est pas.