def somme_chiffres(n):
    somme = 0
    while n != 0:
        somme += n % 10
        n //= 10
    return somme


def harshad(n):
    return n % somme_chiffres(n) == 0


# Tests
assert somme_chiffres(8) == 8
assert somme_chiffres(18) == 9
assert somme_chiffres(409) == 13
assert harshad(18)
assert harshad(72)
assert not harshad(11)
