---
author: Nicolas Revéret
title: Grande Joie (itératif)
tags:
    - 1-boucle
    - 4-maths
---

# Nombres *harshad* (version itérative)

> On s'interdit dans cet exercice d'utiliser `#!py str` et `#!py sum`

Un entier naturel non nul $n$ est dit *harshad*, si $n$ est divisible par la somme des chiffres de $n$ . Le nom de *harshad* a été donné par le mathématicien Dattatreya Ramachandra Kaprekar (1905 - 1986), il signifie "*grande joie*" en sanskrit.

On précise qu'un nombre entier $b$ est un "diviseur" de $a$ si le reste de la division euclidienne de $a$ par $b$ vaut $0$.

Par exemple $18$ est un nombre *harshad* car $1+8$ divise $18$. En effet $18 =2\times (1+8)+0$. 

Vous devez écrire une fonction `harshad(n)` prenant en argument un nombre entier positif `n` et vérifiant que c'est un nombre *harshad*. La fonction renverra `#!py True` ou `#!py False` selon les cas.

Il vous faudra tout d'abord écrire une fonction **itérative** `somme_chiffres(n)` calculant la somme des chiffres de l'entier positif `n`. On n'utilisera que des opérations mathématiques. On s'interdira donc de convertir `n` en une chaîne de caractères.

!!! example "Exemple"

    ```pycon
    >>> somme_chiffres(8)
    8
    >>> somme_chiffres(18)
    9
    >>> somme_chiffres(409)
    13
    >>> harshad(18)
    True
    >>> harshad(72)
    True
    >>> harshad(11)
    False
    ```

Avec Python, on rappelle qu'il est possible de calculer le quotient d'un nombre `n` par `10` en faisant `n // 10`.

De même, `n % 10` renvoie le reste de la division euclidienne de `n` par `10`.

!!! example "Exemple"

    ```pycon
    >>> 409 % 10
    9
    >>> 409 // 10
    40
    >>> 40 % 10
    0
    >>> 40 // 10
    4
    ```


{{ IDE('exo') }}
