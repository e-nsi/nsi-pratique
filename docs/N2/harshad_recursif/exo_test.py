# Tests
assert somme_chiffres(8) == 8
assert somme_chiffres(18) == 9
assert somme_chiffres(409) == 13
assert harshad(18)
assert harshad(72)
assert not harshad(11)

# Tests supplémentaires
HARSHAD = [
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 18, 20, 21, 24, 27, 30,
    36, 40, 42, 45, 48, 50, 54, 60, 63, 70, 72, 80, 81, 84, 90,
    100, 102, 108, 110, 111, 112, 114, 117, 120, 126, 132, 133,
    135, 140, 144, 150, 152, 153, 156, 162, 171, 180, 190, 192,
    195, 198, 200, 201, 204
]
for n in range(1, 1 + max(HARSHAD)):
    assert harshad(n) == (n in HARSHAD), f"Erreur avec {n}"