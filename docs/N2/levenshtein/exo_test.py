assert distance_levenshtein("dis","dis") == 0
assert distance_levenshtein("dis","dit") == 1
assert distance_levenshtein("dis","di") == 1
assert distance_levenshtein("dis","disa") == 1
assert distance_levenshtein("dis","nid") == 2
assert distance_levenshtein("dis","nez") == 3
