---
author: Vincent-Xavier Jumel
title: Distance de Levenshtein
---
# Présentation du problème

Lors de la comparaison de chaines de caractères, on cherche parfois des
chaines plus ou moins proches les unes des autres. En particulier, on peut
définir d'un point de vue mathématique 3 opérations élémentaires permettant
de passer d'une chaine à une autre :

+ substituer un caractère : `"ABC" -> "AAC"` ;
+ insérer un caractère : `"ABC" -> "ABDC"` ;
+ supprimer un caractère : `"ABC" -> "AC"`.

On va supposer ici que le coût de ces opérations est le même. Le nombre
d'opérations élementaires pour passer d'une chaine à une autre constitue la
**distance de Levenshtein**.

On peut utiliser la formule récursive suivante, où on note $\|a\|$ la
longueur de la chaine $a$ :

$$\qquad\operatorname{lev}(a,b) = 
\begin{cases}
  \max(|a|,|b|) & \text{ si } \min(|a|,|b|)=0, \\
  \operatorname{lev}(a-1,b-1) & \text{ si } a[0]=b[0], \\
  1 + \min \begin{cases}
          \operatorname{lev}(a-1,b)\\
          \operatorname{lev}(a,b-1)\\
          \operatorname{lev}(a-1,b-1)
       \end{cases} & \text{ sinon.}
\end{cases}
$$

!!! example "Exemples"

    ```pycon
    >>> distance_levenshtein("distance", "distance")
    0
    >>> distance_levenshtein("distance", "distante")
    1
    >>> distance_levenshtein("distance", "distances")
    1
    >>> distance_levenshtein("distance", "dispense")
    3
    >>> distance_levenshtein("distance", "pense")
    6
    >>> distance_levenshtein("distance", "haricots")
    8
    ```

{{ IDE('exo') }}
