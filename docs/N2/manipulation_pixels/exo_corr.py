def hauteur(image):
    '''renvoie le nombre de lignes de image'''
    return len(image)

def largeur(image):
    '''renvoie la largeur de l'image'''
    return len(image[0])

def negatif(image):
    '''renvoie le négatif de l'image sous la forme d'une liste de listes'''
    # on créé une image de 0 aux mêmes dimensions que le paramètre image
    hauteur_img, largeur_img = hauteur(image), largeur(image)
    nouvelle_image = [[0 for j in range(largeur_img)] for i in range(hauteur_img)]
    for i in range(hauteur_img):
        for j in range(largeur_img):
            nouvelle_image[i][j] = 255 - image[i][j]
    return nouvelle_image

def binaire(image, seuil):
    '''renvoie une image binarisée de l'image sous la forme
    d'une liste de listes contenant des 0 si la valeur
    du pixel est strictement inférieure au seuil
    et 1 sinon'''
    # on crée une image de 0 aux mêmes dimensions que le paramètre image
    hauteur_img, largeur_img = hauteur(image), largeur(image)
    nouvelle_image = [[0 for j in range(largeur_img)] for i in range(hauteur_img)]
    for i in range(hauteur_img):
        for j in range(largeur_img):
            nouvelle_image[i][j] = 0 if image[i][j] < seuil else 1
    return nouvelle_image    
