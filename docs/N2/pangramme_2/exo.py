def indice(minsucule):
    return ord(...) - ord(...)


def minuscule(i):
    return chr(ord(...) + ...)


def lettres_manquantes(phrase):
    presences = [...] * ...
    for caractere in phrase:
        if "a" <= caractere <= ...:
            presences[...] = True

    resultat = []
    for i in range(...):
        if ...:
            resultat.append(...)

    return resultat


# Tests
assert indice("a") == 0
assert indice("z") == 25
assert minuscule(1) == "b"
assert minuscule(24) == "y"
assert lettres_manquantes("portez ce vieux whisky au juge blond qui fume !") == []
assert lettres_manquantes("portez un vieux whisky au juge blond qui fume !") == ["c"]
assert lettres_manquantes("portez ce vieux whisky au juge blond !") == ["f", "m", "q"]
