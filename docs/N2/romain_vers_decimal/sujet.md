---
author: Mireille Coilhac et Franck Chambon
title: Conversion romains décimal
tags:
    - dictionnaire
    - boucle
--- 

# Conversion des chiffres romains en décimal

Les chiffres romains sont un système ancien d’écriture des nombres.

Les chiffres (symboles) romains sont: I, V, X, L, C, D, et M. Ces symboles représentent respectivement 1, 5, 10, 50, 100, 500, et 1000 en base dix.

Cette association pourra être modélisée par un dictionnaire défini une fois pour toute (une constante) donné ci-dessous
```python
VALEUR = {"I": 1, "V": 5, "X": 10, "L": 50, "C": 100, "D": 500, "M": 1000}
```

## Règles de conversion des chiffres romain en décimal :  

Le principe est d'additionner ou retrancher les valeurs de tous les symboles représentant un nombre écrit en chiffres romains.  
1. Si un symbole a une valeur **supérieure ou égale** à celui qui se trouve à sa droite, il est ajouté.

* "XVI" est le nombre 16  car "X" vaut 10 qui est supérieur à 5 qui est la valeur de "V", qui est lui même supérieur à 1 qui est la valeur de "I". Ainsi "XVI" vaut 10 + 5 + 1 = 16  
* "MMXXII" est le nombre 2022, car "M" vaut 1000 qui est supérieur ou égal à 1000, qui est la valeur de "M", qui est lui-même supérieur ou égal à 1 qui est la valeur de "I", qui est lui-même supérieur ou égal à 1 qui est la valeur de "I", qui est lui-même supérieur ou égal à 1 qui est la valeur de "I". Ainsi "MMXXII" vaut 1000 + 1000 + 1 + 1 + 1 = 2022

2. Si un symbole a une valeur **strictement inférieure** à celui qui se trouve à sa droite, il est retranché.

* "CDII" est le nombre 402  car 100 qui est la valeur de C est strictement inférieure  à 500 qui est la valeur de D. Par contre 500 est supérieur ou égal à 1 qui est la valeur de "I", qui est lui-même supérieur ou égal à 1 qui est la valeur de "I". Ainsi "CDII" vaut - 100 + 500 + 1 + 1  = 402  

3. Le symbole le plus à droite est toujours ajouté.

## Travail à réaliser :  
On souhaite créer une fonction  `valeur_romains` qui prend en paramètre une chaîne de caractères (non vide) représentant un nombre écrit en chiffres romains et qui renvoie sa valeur en écriture décimale


!!! example "Exemples"

    ```pycon
    >>> valeur_romains("XVI")
    16
    >>> valeur_romains("MMXXII")
    2022
    >>> valeur_romains("CDII")
    402
    >>> valeur_romains("XLII")
    42
    ```  

Compléter la fonction fournie.

{{ IDE('exo') }}
