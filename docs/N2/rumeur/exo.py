#--- HDR ---#
class File:
    """Classe implémentant une file"""
    def __init__(self, valeurs=None):
        if valeurs is None:
            self.valeurs = []
        else:
            self.valeurs = valeurs

    def est_vide(self):
        return len(self.valeurs) == 0

    def enfiler(self, valeur):
        self.valeurs.append(valeur)

    def defiler(self):
        return self.valeurs.pop(0)
#--- HDR ---#


def distance(graphe, origine, arrivee):
    file = File()
    au_courant = []
    file.enfiler((origine, 0))
    while not file....:
        actuel, nb_etapes = file....
        au_courant.append(...)
        if actuel == arrivee:
            return ...
        for voisin in graphe[...]:
            if voisin not in ...:
                file.enfiler((..., ...))

    # La destination n'est pas atteinte...
    return ...


# Tests
graphe = {'Camille':  ['Romane', 'Marion', 'Paul'],
          'Romane':   ['Nicolas', 'Antoine', 'Paul'],
          'Marion':   ['Camille', 'Romane'],
          'Paul':     ['Antoine', 'Romane'],
          'Antoine':  ['Nicolas'],
          'Nicolas':  ['Camille', 'Antoine'],
          'Stéphane': ['Antoine']}

assert distance(graphe, 'Romane', 'Romane') == 0
assert distance(graphe, 'Romane', 'Antoine') == 1
assert distance(graphe, 'Romane', 'Marion') == 3
assert distance(graphe, 'Romane', 'Stéphane') is None
