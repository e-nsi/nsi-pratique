VALEURS =  {'A': 1, 'B': 3, 'C': 3, 'D': 2, 'E': 1, 'F': 4, 'G': 2, 'H': 4, 
            'I': 1, 'J': 8, 'K': 10, 'L': 1, 'M': 2, 'N': 1, 'O': 1, 'P': 3, 
            'Q': 8, 'R': 1, 'S': 1, 'T': 1, 'U': 1, 'V': 4, 'W': 10, 'X': 10, 
            'Y': 10, 'Z': 10}

BONUS_LETTRE = {'+': 2, '*': 3}
BONUS_MOT = {'#': 2, '@': 3}

def calcul_score(mot, masque):
    score = 0
    multiplicateur_total = 1
    for i in range(len(mot)):
        lettre, symbole_bonus = mot[i], masque[i]
        points_lettre = ...
        if symbole_bonus in BONUS_LETTRE:
            points_lettre = points_lettre * ...
        elif symbole_bonus in ... :
            multiplicateur_total = ...
        score = score + ...
    return ...

# tests
assert calcul_score("GIRAFE","+---#-") == 24
assert calcul_score("KAYAK", "--+--") == 42
assert calcul_score("INFORMATIQUE", "--@---------") == 69
