---
author: Sébastien HOARAU et Fabrice NATIVEL
title: Score d'un mot au Scrabble
tags:
  - boucle
  - dictionnaire
---

# Score d'un mot au Scrabble

Au Scrabble, chaque lettre possède une valeur et le score d'un mot est la somme des valeurs des lettres qui le compose. Ce score peut être modifié par des bonus lors du placement du mot sur le plateau de jeu. Les bonus possibles sont :

* Lettre compte double qui multiplie par 2 la valeur de la lettre positionnée sur cet emplacement,
* Lettre compte triple qui multiplie par 3 la valeur de la lettre positionnée sur cet emplacement,
* Mot compte double qui multiplie par 2 la valeur totale du mot (après application des éventuels bonus de lettre),
* Mot compte triple qui multiplie par 3 la valeur totale du mot (après application des éventuels bonus de lettre).

Une case du plateau ne peut contenir qu'un seul bonus. 

!!! example "Exemple"
    Si on place le mot :

    ![girafe](girafe.svg)

    sur les cases suivantes :

    ![cases](cases.svg)

    Sa valeur est : $\left(2\times2 + 1 + 1 + 1 + 4 + 1 \right) \times 2 = 24$

On modélise les propriétés des cases sur lesquelles sont posées les lettres par les symboles suivants :

* `"+"` indique un bonus lettre compte double,
* `"*"` indique un bonus lettre compte triple,
* `"#"` indique un bonus mot compte double,
* `"@"` indique un bonus mot compte triple.

Les cases sur lesquelles sont posées les lettres d’un mot sont modélisées par une chaine de caractères que l’on appellera « masque ». Dans l'exemple précédent, le masque est la chaine `"+---#-"`. Le caractère `"-"` représente une case sans bonus.


Compléter la définition de la fonction `calcul_score` qui prend en paramètres deux chaines de caractères :

* `mot` dont on veut calculer le score
* `masque` représentant l'emplacement du mot

Cette fonction calcule le score du `mot` en tenant compte des éventuels bonus du `masque`.



{{ IDE('exo') }}
