---
author: Nicolas REVERET
title: Tri par dénombrement
tags:
  - tri
  - boucle
---

# Tri par dénombrement

> Dans cet exercice on s'interdit d'utiliser `max`, `sort` et `sorted`

On souhaite dans cet exercice trier des tableaux de **nombres entiers positifs ou nuls**.

Pour ce faire on utilise l'algorithme du **tri par dénombrement**.

Les étapes sont les suivantes :

* déterminer la valeur maximale dans le tableau,
  
* regrouper dans un autre tableau le nombre d'apparitions de chaque valeur entre 0 et le maximum déterminé à l'étape précédente. Dans ce tableau d'effectifs, la valeur à l'indice `i` donnera le nombre d'apparitions de `i` dans le tableau initial,

* parcourir le tableau des effectifs et ajouter autant de fois que nécessaire chaque indice dans le tableau de nombres triés.

!!! example "Exemple"

    On souhaite trier le tableau `nombres = [3, 3, 2, 0, 3, 0, 2, 0, 2, 3]` :

    * La valeur maximale est `3`

    * Le tableau des effectifs vaut :
  
    ```python
    # indice     0  1  2  3
    effectifs = [3, 0, 3, 4]
    ```

    Le `3` à l'indice `2` signifie que le nombre `2` apparaît trois fois dans `nombres`.
    
    * On parcourt `effectifs` :
        * à l'indice `0` on trouve la valeur `3` : on insère trois fois `0` dans le tableau `nombres_tries`,
        * à l'indice `1` on trouve la valeur `0` : on n'effectue pas d'insertion,
        * à l'indice `2` on trouve la valeur `3` : on insère trois fois `2` dans `nombres_tries`,
        * à l'indice `3` on trouve la valeur `4` : on insère quatre fois `3` dans `nombres_tries`.

    * On obtient le tableau `[0, 0, 0, 2, 2, 2, 3, 3, 3, 3]`.

Vous devez écrire deux fonctions :

* `maximum` prend en argument un tableau de nombres `nombres` et renvoie la valeur maximale dans celui-ci,

* `tri_denombrement` prend en argument un tableau de nombres `nombres` et renvoie un tableau contenant les mêmes valeurs triées dans l'ordre croissant.

!!! example "Exemples"

    ```pycon
    >>> nombres = [4, 5, 4, 2]
    >>> tri_denombrement(nombres)
    [2, 4, 4, 5]
    >>> nombres = [5, 4, 3, 2, 1]
    >>> tri_denombrement(nombres)
    [1, 2, 3, 4, 5]
    >>> nombres = []
    >>> tri_denombrement(nombres)
    []
    ```

{{ IDE('exo', SANS = "max, sorted, list.sort") }}
