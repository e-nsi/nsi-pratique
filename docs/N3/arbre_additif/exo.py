def arbre_additif(valeurs):
    ...


# Tests
assert arbre_additif([6]) == (None, 6, None)
assert arbre_additif([6, 9]) == ((None, 6, None), 15, (None, 9, None))
assert arbre_additif([3, 5, 7, 4]) == (
    ((None, 3, None), 8, (None, 5, None)),
    19,
    ((None, 7, None), 11, (None, 4, None)),
)
