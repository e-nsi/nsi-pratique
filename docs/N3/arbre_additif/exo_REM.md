On procède ici de manière itérative en partant des feuilles.

Une autre approche est de procéder de façon récursive :

```python
def arbre_additif_rec(valeurs):
    if len(valeurs) == 1:
        return (None, valeurs[0], None)

    milieu = len(valeurs) // 2
    return (
        arbre_additif_rec(valeurs[:milieu]),
        sum(valeurs),
        arbre_additif_rec(valeurs[milieu:])
        )
```

Cette façon de procéder présente toutefois plusieurs inconvénients :

* les différentes tranches `valeurs[:milieu]` et `valeurs[milieu:]` occupent de la mémoire,
* les sommes des valeurs sont calculées à plusieurs reprises (lors du première appel on additionne toutes les valeurs, les deux premiers appels récursifs additionnent à nouveau les valeurs du début et de la fin de la liste, *etc*).

