def arbre_additif(valeurs):
    noeuds = [(None, x, None) for x in valeurs]

    while len(noeuds) > 1:
        prochains = []
        for i in range(0, len(noeuds), 2):
            gauche = noeuds[i]
            droite = noeuds[i + 1]
            prochains.append((gauche, gauche[1] + droite[1], droite))
        noeuds = prochains

    return noeuds[0]
