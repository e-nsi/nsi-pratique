# Tests
assert arbre_additif([6]) == (None, 6, None)
assert arbre_additif([6, 9]) == ((None, 6, None), 15, (None, 9, None))
assert arbre_additif([3, 5, 7, 4]) == (
    ((None, 3, None), 8, (None, 5, None)),
    19,
    ((None, 7, None), 11, (None, 4, None)),
)


# Tests supplémentaires
from random import randrange

def __arbre_additif__(valeurs):
    noeuds = [(None, x, None) for x in valeurs]

    while len(noeuds) > 1:
        prochains = []
        for i in range(0, len(noeuds), 2):
            gauche = noeuds[i]
            droite = noeuds[i + 1]
            prochains.append((gauche, gauche[1] + droite[1], droite))
        noeuds = prochains

    return noeuds[0]

for _ in range(10):
    nb_feuilles = 2 ** randrange(3, 10)
    valeurs = [randrange(-100, 101) for _ in range(nb_feuilles)]
    attendu = __arbre_additif__(valeurs)
    assert arbre_additif(valeurs) == attendu, f"Erreur avec {valeurs}"