---
author: Nicolas Revéret
title: Arbre binaire additif
tags:
  - 2-tuple
  - 8-arbre
---

# Arbre binaire additif

!!! note "Représentation des arbres"

    On représente les arbres binaires ainsi :

    - l'arbre vide est représenté par `#!py None`,

    - un arbre non vide est représenté par un tuple `(sous-arbre gauche, valeur, sous-arbre droit)`.

    Ainsi le tuple `((None, 6, None), 15, None)` représente l'arbre suivant :

    ```mermaid
    graph TD
        R(15) --> A(6)
        R --> B( )
        A --> C( )
        A --> D( )
    ```

On souhaite construire des arbres binaires parfaits « *additifs* ». Dans ces arbres, la valeur d'un nœud intérieur est toujours égale à la somme des valeurs de ses enfants.

```mermaid
graph TD
    R(19) --> A(8)
    R --> B(11)
    A --> C(3)
    A --> D(5)
    B --> E(7)
    B --> F(4)
    C --> G( )
    C --> H( )
    D --> I( )
    D --> J( )
    E --> K( )
    E --> L( )
    F --> M( )
    F --> N( )
```

!!! tip "Arbre binaire parfait"

    On rappelle qu'un arbre binaire est dit **parfait** lorsque tous ses niveaux sont complètement remplis.

    On peut en déduire qu'un arbre binaire parfait de hauteur $h$ compte $2^{h-1}$ feuilles.


On demande d'écrire la fonction `arbre_additif` prenant en argument la liste `valeurs` contenant les valeurs des feuilles et renvoyant l'arbre correspondant.

On garantit que le nombre d'éléments de `valeurs` est une puissance de $2$. Ces éléments sont tous des nombres entiers.

!!! example "Exemples"

    ```pycon
    >>> arbre_additif([6])
    (None, 6, None)
    >>> arbre_additif([6, 9])
    ((None, 6, None), 15, (None, 9, None))
    >>> arbre_additif([3, 5, 7, 4])
    (((None, 3, None), 8, (None, 5, None)), 19, ((None, 7, None), 11, (None, 4, None)))
    ```

{{ IDE('exo') }}