def correspond(chaine, regles, debut, fin):
    ...


# tests
regles = {(0, "a"): 1, (0, "b"): 2, (1, "a"): 2, (1, "b"): 1, (1, "c"): 0}
debut = 0
fin = 2

assert correspond("b", regles, debut, fin) == True
assert correspond("abba", regles, debut, fin) == True
assert correspond("acaa", regles, debut, fin) == True
assert correspond("abbbcaa", regles, debut, fin) == True
assert correspond("abbc", regles, debut, fin) == False
assert correspond("abbbd", regles, debut, fin) == False
assert correspond("ba", regles, debut, fin) == False
