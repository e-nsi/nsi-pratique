# --- HDR ---#
class Expression:
    def __init__(self, *args):
        self.args = args

    def __repr__(self):
        args = ", ".join(map(repr, self.args))
        return f"{self.__class__.__name__}({args})"

    def __eq__(self, autre):
        return type(self) is type(autre) and self.args == autre.args


class Nombre(Expression):
    pass


class Racine(Expression):
    pass


class Somme(Expression):
    pass


class Difference(Expression):
    pass


class Produit(Expression):
    pass


class Quotient(Expression):
    pass


class Fonction(Expression):
    pass


# --- HDR ---#


operateurs = {
    "racine": (Racine, 1),
    "+": (Somme, 2),
    "-": (Difference, 2),
    "*": (Produit, 2),
    "/": (Quotient, 2),
    "fonction": (Fonction, 3),
}


def arbre_syntaxique(postfix):
    pile = []
    for jeton in postfix:
        if jeton in operateurs:
            classe, arite = operateurs[jeton]
            if arite == 1:
                pile.append(classe(pile.pop()))
            elif arite == 2:
                b = pile.pop()
                a = pile.pop()
                pile.append(classe(a, b))
            else:
                c = pile.pop()
                b = pile.pop()
                a = pile.pop()
                pile.append(classe(a, b, c))
        else:
            pile.append(Nombre(jeton))
    return pile.pop()


def arbre_syntaxique_alt(postfix):
    pile = []
    for jeton in postfix:
        if jeton in operateurs:
            classe, arite = operateurs[jeton]
            operandes = reversed([pile.pop() for _ in range(arite)])
            pile.append(classe(*operandes))
        else:
            pile.append(Nombre(jeton))
    return pile.pop()


# Tests
assert arbre_syntaxique([9, "racine"]) == Racine(Nombre(9))
assert arbre_syntaxique([3, 8, "+"]) == Somme(Nombre(3), Nombre(8))
assert arbre_syntaxique([1, 2, 3, "fonction"]) == Fonction(
    Nombre(1), Nombre(2), Nombre(3)
)
assert arbre_syntaxique([3, 8, 12, "*", "+"]) == (
    Somme(Nombre(3), Produit(Nombre(8), Nombre(12)))
)
assert arbre_syntaxique([1, 2, 3, "fonction", 5, "/"]) == (
    Quotient(Fonction(Nombre(1), Nombre(2), Nombre(3)), Nombre(5))
)

# Tests supplémentaires
from random import choice, randrange

pile = []
N = randrange(10, 20)
UNS = [(Racine, 1)]
DEUX = [(Somme, 2), (Difference, 2), (Produit, 2), (Quotient, 2)]
TROIS = [(Fonction, 3)]
for _ in range(N):
    if len(pile) >= 3:
        classe, arite = choice(UNS + DEUX + TROIS + [(Nombre, 1)])
        operandes = reversed([pile.pop() for _ in range(arite)])
        pile.append(classe(*operandes))
    elif len(pile) == 2:
        classe, arite = choice(UNS + DEUX + [(Nombre, 1)])
        operandes = reversed([pile.pop() for _ in range(arite)])
        pile.append(classe(*operandes))
    elif len(pile) == 1:
        classe, arite = choice(UNS + [(Nombre, 1)])
        operandes = reversed([pile.pop() for _ in range(arite)])
        pile.append(classe(*operandes))
    else:
        pile.append(Nombre(randrange(1, 21)))
while len(pile) > 1:
    if len(pile) >= 3:
        classe, arite = choice(UNS + DEUX + TROIS)
        operandes = reversed([pile.pop() for _ in range(arite)])
        pile.append(classe(*operandes))
    elif len(pile) == 2:
        classe, arite = choice(UNS + DEUX)
        operandes = reversed([pile.pop() for _ in range(arite)])
        pile.append(classe(*operandes))
    elif len(pile) == 1:
        classe, arite = choice(UNS)
        operandes = reversed([pile.pop() for _ in range(arite)])
        pile.append(classe(*operandes))

print(pile[0])
