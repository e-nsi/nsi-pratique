def correspond(chaine, regles, debut, fin):
    etat = debut
    indice = 0
    taille = len(chaine)
    while indice < taille:
        caractere = chaine[indice]
        if (etat, caractere) in regles:
            etat = regles[(etat, caractere)]
            indice += 1
        else:
            return False
    return etat == fin


# tests
# Expression régulière cherchée : (ab*c)*aa|b
regles = {(0, "a"): 1, (0, "b"): 2, (1, "a"): 2, (1, "b"): 1, (1, "c"): 0}
debut = 0
fin = 2

correspond("b", regles, debut, fin) == True
correspond("abbcaa", regles, debut, fin) == True
correspond("abbcb", regles, debut, fin) == True
correspond("abbc", regles, debut, fin) == False
correspond("abbbd", regles, debut, fin) == False
correspond("ba", regles, debut, fin) == False
