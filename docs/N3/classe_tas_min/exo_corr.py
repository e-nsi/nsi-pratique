class TasMin:
    def __init__(self):
        self.donnees = [None]
        self.effectif = 0

    def est_vide(self):
        return self.effectif == 0

    def echange(self, i, j):
        temp = self.donnees[i]
        self.donnees[i] = self.donnees[j]
        self.donnees[j] = temp

    def ajoute(self, element):
        self.donnees.append(element)
        self.effectif += 1
        i = self.effectif
        i_parent = i // 2
        while (i > 1) and (self.donnees[i_parent] > self.donnees[i]):
            self.echange(i_parent, i)
            i = i_parent
            i_parent = i // 2

    def extrait_min(self):
        if self.est_vide():
            raise IndexError("Tas vide ; impossible d'extraire le minimum")

        if self.effectif == 1:
            self.effectif = 0
            return self.donnees.pop()

        mini = self.donnees[1]  # l'élément minimal à renvoyer à la fin

        # On place à la racine le dernier élément
        self.donnees[1] = self.donnees.pop()
        self.effectif -= 1

        # On va le remettre à une place qui respecte la règle
        i_noeud = 1
        i_prochain = 0
        while i_noeud != i_prochain:
            i_prochain = i_noeud
            if 2 * i_noeud <= self.effectif:  # il existe un enfant gauche
                if self.donnees[2 * i_noeud] < self.donnees[i_noeud]:
                    i_prochain = 2 * i_noeud  # on peut l'échanger avec l'enfant gauche
            if 2 * i_noeud + 1 <= self.effectif:  # il existe un enfant droit
                if self.donnees[2 * i_noeud + 1] < self.donnees[i_prochain]:
                    i_prochain = 2 * i_noeud + 1  # on peut l'échanger avec l'enfant droit
            if i_prochain != i_noeud:
                self.echange(i_noeud, i_prochain)
                i_noeud = i_prochain
                i_prochain = 0
        # fin
        return mini


# tests
test = TasMin()

## test est_vide
assert test.est_vide()

## test un ajoute/extrait
univers = 42
test.ajoute(univers)
element = test.extrait_min()
assert element == univers, "On doit retrouver 42"
assert test.est_vide(), "Le tas doit être vide"

## test plusieurs ajout/extrait
###  dans l'ordre
premiers = [2, 3, 5, 7, 11]
nb_premiers = len(premiers)

for p in premiers:
    test.ajoute(p)
assert [test.extrait_min() for _ in range(nb_premiers)] == premiers
assert test.est_vide()

###  dans l'ordre inverse
for p in reversed(premiers):
    test.ajoute(p)
assert [test.extrait_min() for _ in range(nb_premiers)] == premiers
assert test.est_vide()

###  dans le désordre
from random import shuffle

bazar = premiers.copy()
for _ in range(100):
    shuffle(bazar)
    for élément in bazar:
        test.ajoute(élément)
    shuffle(bazar)
    assert [test.extrait_min() for _ in range(nb_premiers)] == premiers
    assert test.est_vide()
