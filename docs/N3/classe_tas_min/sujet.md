---
author: Franck Chambon, Nicolas Revéret
title: Classe Tas Min
tags:
  - 8-arbre
  - 7-POO
---

# Classe `TasMin`

Un *Tas-Min* est un arbre binaire vérifiant les propriétés suivantes :

* c'est un arbre binaire parfait (tous ses niveaux sont complets sauf éventuellement le dernier qui, dans ce cas, est rempli de gauche à droite) ;
* chaque nœud possède une étiquette. Toutes les étiquettes sont comparables entre elles ;
* chaque nœud a une étiquette strictement inférieure à celle de ses éventuels enfants ;

Cette dernière propriété assure que la racine d'un Tas-Min est le nœud dont l'étiquette est minimale.

```mermaid
graph TD
    R{12} --> N1{13}
    R     --> N2{53}
    N1    --> N3{65}
    N1    --> N4{20}
    N2    --> N5{61}
    N2    -.-> N6( )
```

L'objectif de cet exercice est la création d'une classe `TasMin`. On utilisera la modélisation sous forme d'un tableau redimensionnable. Dans cette modélisation, avec Python :

* le données sont stockées dans un tableau dont la première valeur (à l'indice `#!py 0`) est `#!py None`,
* l'étiquette de la racine est stockée à l'indice `#!py 1`,
* si l'étiquette d'un nœud est stockée à l'indice `#!py i` :
    * celle de son enfant gauche, s'il existe, est à l'indice `#!py 2 * i`,
    * celle de son enfant droit, s'il existe, est à l'indice `#!py 2 * i + 1`,

!!! info "Tableau redimensionnable"

    Pour cet exercice, on pourra utiliser le type `#!py list` de Python, avec les méthodes suivantes : 
    
    * création d'une liste,
    * ajout d'un élément à la fin de la liste,
    * suppression et récupération du dernier élément, 
    * accès en lecture et écriture aux éléments avec l'indice donné.

    On n'utilisera **pas** les tranches, ni aucune fonction native de tri.

On attend une classe `TasMin` avec les méthodes suivantes :

- Initialisation : on crée un tableau redimensionnable `donnees` qui ne contient que `None` en unique élément, on crée un attribut `effectif` égal à zéro.
- `est_vide()` : détermine si le Tas-Min est vide en renvoyant un booléen.
- `echange(i, j)` : échange les valeur d'indices `#!py i` et `#!py j` dans le tableau `#!py donnees`.
- `ajoute(element)` : 
    - on augmente `effectif` de 1,
    - on ajoute `element` en fin de `donnees`,
    - on échange les éléments nécessaires pour garder une structure de Tas-Min. `element` remonte la structure arborescente, en échangeant avec son ancêtre, jusqu'à que la structure soit correcte.
- `extrait_min()` :
    - on enregistre la valeur minimale des données, à l'indice 1, pour la renvoyer en fin de traitement,
    - on diminue `effectif` de 1,
    - on extrait le dernier élément de `donnees` pour le placer à l'indice 1,
    - on échange les éléments nécessaires pour garder une structure de Tas-Min. On commence par échanger l'élément remonté avec le plus petit de ses enfants et l'on poursuit le procédé dans la branche.
- On ne fera pas la vérification que l'utilisateur utilise toujours des données comparables entre elles.

On donne un exemple commenté pour chacune des deux dernières méthodes.

!!! example "Ajouter un élément - exemple"

    On suppose qu'on dispose d'un Tas-Min, et que l'on souhaite y ajouter l'élément $17$.

    === "Le Tas-Min initial"

        ```mermaid
        graph TD
            R{10} --> N1{42}
            R     --> N2{23}
            N1    --> N3{55}
            N1    --> N4{67}
            N2    --> N5{31}
            N2    --> N6{26}
            N3    --> N7{60}
            N3    --> N8{59}
            N4    --> N9{88}
            N4    --> N10{83}
            N5    -.-> N11( )
            N5    -.-> N12( )
            N6    -.-> N13( )
            N6    -.-> N14( )
        ```

        Modélisation : `[None, 10, 42, 23, 55, 67, 31, 26, 60, 59, 88, 83]`

    === "Ajout de $17$ à la fin"

        ```mermaid
        graph TD
            R{10} --> N1{42}
            R     --> N2{23}
            N1    --> N3{55}
            N1    --> N4{67}
            N2    --> N5{31}
            N2    --> N6{26}
            N3    --> N7{60}
            N3    --> N8{59}
            N4    --> N9{88}
            N4    --> N10{83}
            N5    --> N11{17}
            N5    -.-> N12( )
            N6    -.-> N13( )
            N6    -.-> N14( )

            classDef marque fill:#f77,stroke:#333,stroke-width:4px;
            class N11 marque;
        ```

        Modélisation : `[None, 10, 42, 23, 55, 67, 31, 26, 60, 59, 88, 83, 17]`

        L'effectif a augmenté de 1.

    === "Échange $17$ et $31$"

        ```mermaid
        graph TD
            R{10} --> N1{42}
            R     --> N2{23}
            N1    --> N3{55}
            N1    --> N4{67}
            N2    --> N5{17}
            N2    --> N6{26}
            N3    --> N7{60}
            N3    --> N8{59}
            N4    --> N9{88}
            N4    --> N10{83}
            N5    --> N11{31}
            N5    -.-> N12( )
            N6    -.-> N13( )
            N6    -.-> N14( )
            
            classDef marque fill:#f77,stroke:#333,stroke-width:4px;
            class N5,N11 marque;
        ```

        > Modélisation : `[None, 10, 42, 23, 55, 67, 17, 26, 60, 59, 88, 83, 31]`

    === "Échange $17$ et $23$"

        ```mermaid
        graph TD
            R{10} --> N1{42}
            R     --> N2{17}
            N1    --> N3{55}
            N1    --> N4{67}
            N2    --> N5{23}
            N2    --> N6{26}
            N3    --> N7{60}
            N3    --> N8{59}
            N4    --> N9{88}
            N4    --> N10{83}
            N5    --> N11{31}
            N5    -.-> N12( )
            N6    -.-> N13( )
            N6    -.-> N14( )
            
            classDef marque fill:#f77,stroke:#333,stroke-width:4px;
            class N5,N2 marque;
        ```

        > Modélisation : `[None, 10, 42, 17, 55, 67, 23, 26, 60, 59, 88, 83, 31]`


        On obtient un Tas-Min.


!!! example "Extraire le minimum - exemple"

    On suppose qu'on dispose d'un Tas-Min, et que l'on souhaite extraire le minimum.

    === "Le Tas-Min initial"

        ```mermaid
        graph TD
            R{10} --> N1{42}
            R     --> N2{23}
            N1    --> N3{55}
            N1    --> N4{67}
            N2    --> N5{31}
            N2    --> N6{26}
            N3    --> N7{60}
            N3    --> N8{59}
            N4    --> N9{88}
            N4    --> N10{83}
            N5    -.-> N11( )
            N5    -.-> N12( )
            N6    -.-> N13( )
            N6    -.-> N14( )
        ```

        Modélisation : `[None, 10, 42, 23, 55, 67, 31, 26, 60, 59, 88, 83]`

    === "Le dernier passe premier"

        ```mermaid
        graph TD
            R{83} --> N1{42}
            R     --> N2{23}
            N1    --> N3{55}
            N1    --> N4{67}
            N2    --> N5{31}
            N2    --> N6{26}
            N3    --> N7{60}
            N3    --> N8{59}
            N4    --> N9{88}
            N4    -.-> N10( )
            N5    -.-> N11( )
            N5    -.-> N12( )
            N6    -.-> N13( )
            N6    -.-> N14( )
            
            classDef marque fill:#f77,stroke:#333,stroke-width:4px;
            class R,N10 marque;
        ```

        Modélisation : `[None, 83, 42, 23, 55, 67, 31, 26, 60, 59, 88]`

        L'effectif diminue de 1.

        On a pris soin de sauvegarder $10$ dans une variable.

    === "Échange $83$ et $23$"

        ```mermaid
        graph TD
            R{23} --> N1{42}
            R     --> N2{83}
            N1    --> N3{55}
            N1    --> N4{67}
            N2    --> N5{31}
            N2    --> N6{26}
            N3    --> N7{60}
            N3    --> N8{59}
            N4    --> N9{88}
            N4    -.-> N10( )
            N5    -.-> N11( )
            N5    -.-> N12( )
            N6    -.-> N13( )
            N6    -.-> N14( )
            
            classDef marque fill:#f77,stroke:#333,stroke-width:4px;
            class R,N2 marque;
        ```

        Modélisation : `[None, 23, 42, 83, 55, 67, 31, 26, 60, 59, 88]`


    === "Échange $83$ et $26$"

        ```mermaid
        graph TD
            R{23} --> N1{42}
            R     --> N2{26}
            N1    --> N3{55}
            N1    --> N4{67}
            N2    --> N5{31}
            N2    --> N6{83}
            N3    --> N7{60}
            N3    --> N8{59}
            N4    --> N9{88}
            N4    -.-> N10( )
            N5    -.-> N11( )
            N5    -.-> N12( )
            N6    -.-> N13( )
            N6    -.-> N14( )
            
            classDef marque fill:#f77,stroke:#333,stroke-width:4px;
            class N6,N2 marque;
        ```

        Modélisation : `[None, 23, 42, 26, 55, 67, 31, 83, 60, 59, 88]`

        On obtient un Tas-Min. On peut renvoyer la valeur sauvegardée : $10$.

!!! warning "Difficultés principales"

    Quand on cherche à extraire le minimum, on passe le dernier en premier, puis on le fait redescendre (de manière répétitive) à la place de son plus petit enfant.
    
    Trois cas peuvent se produire :

    - il n'y a pas d'enfants : l'élément est à la bonne position ;
    - il y a un enfant à gauche ;
    - il y a aussi un enfant à droite ;

    Il existe une condition simple entre l'indice d'un nœud et `effectif` pour savoir si ce nœud a un, deux ou aucun enfant.

    On traitera le cas d'effectif 1 à part.

Compléter le code :

{{ IDE('exo', MAX=1000) }}

