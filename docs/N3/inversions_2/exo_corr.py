def inversions(tab):
    if len(tab) <= 1:
        return 0

    N = len(tab)
    gauche = tab[: N // 2]
    droite = tab[N // 2 :]
    inv_gauche = inversions(gauche)
    inv_droite = inversions(droite)

    total = 0
    i_gauche = 0
    i_droite = 0
    i_tab = 0

    while i_gauche < len(gauche) and i_droite < len(droite):
        if gauche[i_gauche] <= droite[i_droite]:
            tab[i_tab] = gauche[i_gauche]
            i_gauche += 1
        else:
            total += len(gauche) - i_gauche
            tab[i_tab] = droite[i_droite]
            i_droite += 1
        i_tab += 1

    while i_gauche < len(gauche):
        tab[i_tab] = gauche[i_gauche]
        i_gauche += 1
        i_tab += 1

    while i_droite < len(droite):
        tab[i_tab] = droite[i_droite]
        i_droite += 1
        i_tab += 1

    return inv_gauche + inv_droite + total


# Tests
assert inversions([]) == 0
assert inversions([5, 6, 7, 9]) == 0
assert inversions([7, 5, 9, 6]) == 3