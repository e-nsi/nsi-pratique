def l_max_equilibree(bits):
    ...



# tests
assert l_max_equilibree([0, 0, 1, 0, 0, 0, 1, 1, 0, 0]) == 6

assert l_max_equilibree([1, 1, 1, 1]) == 0

assert l_max_equilibree([1, 0, 1, 0, 1]) == 4
