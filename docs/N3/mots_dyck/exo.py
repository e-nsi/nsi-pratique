def mots_dyck(n):
    ...


# Tests
assert mots_dyck(0) == [""]
assert mots_dyck(1) == ["()"]
assert sorted(mots_dyck(2)) == ["(())", "()()"]
assert sorted(mots_dyck(3)) == ["((()))", "(()())", "(())()", "()(())", "()()()"]
