def mots_dyck(n):
    longueur = 2 * n
    mots = []
    pile = [("", 0, 0)]
    while len(pile) > 0:
        mot, ouvrantes, fermantes = pile.pop()
        if len(mot) == longueur:
            mots.append(mot)
        else:
            if ouvrantes < n:
                pile.append((mot + "(", ouvrantes + 1, fermantes))
            if ouvrantes - fermantes > 0:
                pile.append((mot + ")", ouvrantes, fermantes + 1))
    return mots


# Tests
assert mots_dyck(0) == [""]
assert mots_dyck(1) == ["()"]
assert sorted(mots_dyck(2)) == ["(())", "()()"]
assert sorted(mots_dyck(3)) == ["((()))", "(()())", "(())()", "()(())", "()()()"]
