def rang(mot):
    ...


# Tests
assert rang("GALA") == 8, "Erreur sur le rang de GALA"
assert rang("BCA") == 4, "Erreur sur le rang de BCA"
assert rang("ABAACAC") == 32, "Erreur sur le rang de ABAACAC"
assert rang("ENUMERATION") == 1971860, "Erreur sur le rang de ENUMERATION"
