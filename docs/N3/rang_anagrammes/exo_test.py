# Tests
assert rang("GALA") == 8, "Erreur sur le rang de GALA"
assert rang("BCA") == 4, "Erreur sur le rang de BCA"
assert rang("ABAACAC") == 32, "Erreur sur le rang de ABAACAC"
assert rang("ENUMERATION") == 1971860, "Erreur sur le rang de ENUMERATION"

# Tests supplémentaires
from math import factorial
from string import ascii_uppercase as ALPHABET
from random import choice, randrange


def rang_corr(mot):
    if len(mot) == 1:
        return 1

    repetitions = {}
    for lettre in mot:
        repetitions[lettre] = repetitions.get(lettre, 0) + 1
    uniques = set(mot)
    uniques_triees = sorted(uniques)
    total = 0
    i = 0
    while uniques_triees[i] != mot[0]:
        debut = uniques_triees[i]
        denominateur = factorial(repetitions[debut] - 1)
        for c in uniques - {debut}:
            denominateur *= factorial(repetitions[c])
        total += factorial(len(mot) - 1) // denominateur
        i += 1
    return total + rang_corr(mot[1:])


for _ in range(20):
    taille = randrange(5, 21)
    mot = "".join([choice(ALPHABET) for _ in range(taille)])
    attendu = rang_corr(mot)
    assert rang(mot) == attendu, f"Erreur avec {mot}"
