from heapq import heappush, heappop

class Tas_Min():
    def __init__(self):
        self.tas = []
        
    def ajoute(self, element):
        heappush(self.tas, element)
        
    def extrait_mini(self):
        return heappop(self.tas)

def travaille(grille):
    n = len(grille)
    m = len(grille[0])

    entree = [(i, 0) for i in range(n)]
    
    def est_sortie(i, j):
        return (j == m - 1)
    
    INFINI = float('inf')
    scores = [[(INFINI, 0, '') for _ in range(m)] for _ in range(n)]

    def est_valide(i, j):
        return (0 <= i < n) and (0 <= j < m)
    
    def traite(score, i, j, nouvelle_direction):
        poids, longueur, direction = score
        nouveau_poids = max(poids, grille[i][j])
        nouveau_score = (nouveau_poids, longueur + 1, nouvelle_direction)
        if nouveau_score < scores[i][j]:
            frontiere.ajoute((nouveau_score, i, j))
            scores[i][j] = nouveau_score
        
    def voisins(i, j):
        return [ # indique la direction retour !
            (i-1, j, 'S'), (i+1, j, 'N'),
            (i, j+1, 'O'), (i, j-1, 'E'),
            (i-1, j+1, 'SO'), (i+1, j-1, 'NE'),
            (i+1, j+1, 'NO'), (i-1, j-1, 'SE'),
        ]
    
    frontiere = Tas_Min()
    for (i, j) in entree:
        SCORE_NUL = (0, 0, '')
        traite(SCORE_NUL, i, j, '')

    while True:
        score, i, j = frontiere.extrait_mini()
        if est_sortie(i, j):
            return scores, (i, j)
        for (i_, j_, direction) in voisins(i, j):
            if est_valide(i_, j_):
                traite(score, i_, j_, direction)

def shallowest_path(river):
    retour = {
        'N': (-1, 0), 'S': (+1, 0),
        'E': (0, +1), 'O': (0, -1),
        'NO': (-1, -1), 'SE': (+1, +1),
        'NE': (-1, +1), 'SO': (+1, -1),
        }
    scores, (i, j) = travaille(river)
    poids, longueur, direction = scores[i][j]
    reponse = [(i, j)]
    while direction != '':
        di, dj = retour[direction]
        i, j = i + di, j+ dj
        score, longueur, direction = scores[i][j]
        reponse.append((i, j))
    reponse.reverse()
    return reponse


river = [
    [2, 3, 2],
    [1, 1, 4],
    [9, 5, 2],
    [1, 4, 4],
    [1, 5, 4],
    [2, 1, 4],
    [5, 1, 2],
    [5, 5, 5],
    [8, 1, 9],
]

print(shallowest_path(river))
