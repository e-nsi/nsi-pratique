class Sudoku:
    """Classe pour le puzzle Sudoku"""
    
    def __init__(self, texte):
        self.grille = ...
    
    def est_resolu(self):
        ...
        return ...

    def __repr__(self):
        sortie = "Sudoku('"
        ...
        return sortie + "')"

    def __str__(self):
        barre = "+---+---+---+\n"
        
        sortie = ""
        for i in range(9):
            if i % 3 == 0:
                sortie += barre
            ...
        return sortie + barre
    



# Tests

diabolique = Sudoku(
    '37...92....6.....55..27..4.7..6....2....9....6....3..7.6..28..44.....1....91...56'
)

diabolique_tab = [
    [3, 7, 0, 0, 0, 9, 2, 0, 0],
    [0, 0, 6, 0, 0, 0, 0, 0, 5],
    [5, 0, 0, 2, 7, 0, 0, 4, 0],
    [7, 0, 0, 6, 0, 0, 0, 0, 2],
    [0, 0, 0, 0, 9, 0, 0, 0, 0],
    [6, 0, 0, 0, 0, 3, 0, 0, 7],
    [0, 6, 0, 0, 2, 8, 0, 0, 4],
    [4, 0, 0, 0, 0, 0, 1, 0, 0],
    [0, 0, 9, 1, 0, 0, 0, 5, 6],
]

for i in range(9):
    # chaque ligne contient les mêmes valeurs
    assert diabolique.grille[i] == diabolique_tab[i], f"erreur, ligne {i=}"
# Ceci permet de valider la méthode __init__



assert diabolique.est_resolu() is False



assert repr(diabolique) == "Sudoku('37...92....6.....55..27..4.7..6....2....9....6....3..7.6..28..44.....1....91...56')"
# Ceci permet de valider la méthode __repr__



diabolique_str = """\
+---+---+---+
|37.|..9|2..|
|..6|...|..5|
|5..|27.|.4.|
+---+---+---+
|7..|6..|..2|
|...|.9.|...|
|6..|..3|..7|
+---+---+---+
|.6.|.28|..4|
|4..|...|1..|
|..9|1..|.56|
+---+---+---+
"""
assert str(diabolique) == diabolique_str
# Ceci permet de valider la méthode __str__

