## Commentaires

{{ IDE('exo_corr_1') }}

On peut factoriser le code avec des fonctions de classe.

```python
class Sudoku:
    """Classe pour le puzzle Sudoku"""

    def chiffre(caractere):
        """Renvoie le chiffre associé à la représentation textuelle"""
        ...

    def ligne(k):
        """Renvoie toutes les coordonnées de la ligne k"""
        ...

    def colonne(k):
        """Renvoie toutes les coordonnées de la colonne j"""
        ...

    def region(k):
        """Renvoie toutes les coordonnées de la region n°k"""
        ...

    def num_region(i, j):
        """Renvoie le numéro de la région de (i, j)"""
        ...
```

On les utilise ainsi `#!py Sudoku.chiffre('4')` qui renvoie `4`.

Retrouver un meilleur code dans le corrigé de la partie 3.
