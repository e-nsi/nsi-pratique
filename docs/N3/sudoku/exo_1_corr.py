class Sudoku:
    """Classe pour le puzzle Sudoku"""
    
    def __init__(self, texte):
        self.grille = []
        for i in range(9):
            ligne = []
            for j in range(9):
                caractere = texte[9*i + j]
                if caractere == '.':
                    chiffre = 0
                else:
                    chiffre = int(caractere)
                ligne.append(chiffre)
            self.grille.append(ligne)
    
    def est_resolu(self):
        # vérification des lignes
        for i in range(9):
            present = [False] * 9
            for j in range(9):
                present[self.grille[i][j] - 1] = True
            if present != [True] * 9:
                return False

        # vérification des colonnes
        for j in range(9):
            present = [False] * 9
            for i in range(9):
                present[self.grille[i][j] - 1] = True
            if present != [True] * 9:
                return False

        # vérification des régions
        for k in range(9):
            present = [False] * 9
            i_0 = 3 * (k // 3)
            j_0 = 3 * (k % 3)
            for i in range(3):
                for j in range(3):
                    present[self.grille[i_0 + i][j_0 + j] - 1] = True
            if present != [True] * 9:
                return False
        
        return True
    
    def __repr__(self):
        sortie = "Sudoku('"
        for i in range(9):
            for j in range(9):
                chiffre = self.grille[i][j]
                if chiffre == 0:
                    sortie += '.'
                else:
                    sortie += str(chiffre)
        return sortie + "')"

    def __str__(self):
        barre = "+---+---+---+\n"
        sortie = ""
        for i in range(9):
            if i % 3 == 0:
                sortie += barre
            for j in range(9):
                if j % 3 == 0:
                    sortie += "|"
                chiffre = self.grille[i][j]
                if chiffre == 0:
                    sortie += '.'
                else:
                    sortie += str(chiffre)
            sortie += "|\n"
        return sortie + barre



# Tests

diabolique = Sudoku(
    '37...92....6.....55..27..4.7..6....2....9....6....3..7.6..28..44.....1....91...56'
)

diabolique_tab = [
    [3, 7, 0, 0, 0, 9, 2, 0, 0],
    [0, 0, 6, 0, 0, 0, 0, 0, 5],
    [5, 0, 0, 2, 7, 0, 0, 4, 0],
    [7, 0, 0, 6, 0, 0, 0, 0, 2],
    [0, 0, 0, 0, 9, 0, 0, 0, 0],
    [6, 0, 0, 0, 0, 3, 0, 0, 7],
    [0, 6, 0, 0, 2, 8, 0, 0, 4],
    [4, 0, 0, 0, 0, 0, 1, 0, 0],
    [0, 0, 9, 1, 0, 0, 0, 5, 6],
]

for i in range(9):
    # chaque ligne contient les mêmes valeurs
    assert diabolique.grille[i] == diabolique_tab[i], f"erreur, ligne {i=}"
# Ceci permet de valider la méthode __init__



assert diabolique.est_resolu() is False



assert repr(diabolique) == "Sudoku('37...92....6.....55..27..4.7..6....2....9....6....3..7.6..28..44.....1....91...56')"
# Ceci permet de valider la méthode __repr__



diabolique_str = """\
+---+---+---+
|37.|..9|2..|
|..6|...|..5|
|5..|27.|.4.|
+---+---+---+
|7..|6..|..2|
|...|.9.|...|
|6..|..3|..7|
+---+---+---+
|.6.|.28|..4|
|4..|...|1..|
|..9|1..|.56|
+---+---+---+
"""

assert str(diabolique) == diabolique_str
# Ceci permet de valider la méthode __str__
