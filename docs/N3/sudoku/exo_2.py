class Sudoku:
    """Classe pour le puzzle Sudoku"""

    def __init__(self, texte):
        self.grille = ...
    
    def chiffres_possibles(self, i, j):
        ...
    
    def bon_choix(self):
        ...




# Tests

diabolique = Sudoku('37...92....6.....55..27..4.7..6....2....9....6....3..7.6..28..44.....1....91...56')

possibles = [
    None,
    [1, 2, 4, 8, 9],
    [1, 8],
    None,
    None,
    None,
    [3, 5, 7],
    [3, 6, 7, 8],
    None,
]

for k in range(9):
    if possibles[k] is not None:
        assert diabolique.chiffres_possibles(k, k) == possibles[k]

assert diabolique.bon_choix() in [(2, 5), (6, 0)]

