class Sudoku:
    """Classe pour le puzzle Sudoku"""

    def __init__(self, texte):
        self.grille = []
        for i in range(9):
            ligne = []
            for j in range(9):
                caractere = texte[9*i + j]
                if caractere == '.':
                    chiffre = 0
                else:
                    chiffre = int(caractere)
                ligne.append(chiffre)
            self.grille.append(ligne)
    
    def chiffres_possibles(self, i_0, j_0):
        assert self.grille[i_0][j_0] == 0
        possible = [True] * 10
        
        # lecture de la ligne i_0
        for j in range(9):
            chiffre = self.grille[i_0][j]
            if chiffre > 0:
                possible[chiffre] = False
        # lecture de la colonne j_0
        for i in range(9):
            chiffre = self.grille[i][j_0]
            if chiffre > 0:
                possible[chiffre] = False
        # lecture de la région de (i_0, j_0)
        k = 3 * (i_0 // 3) + j_0 // 3
        i_0 = 3 * (k // 3)
        j_0 = 3 * (k % 3)
        for i in range(3):
            for j in range(3):
                chiffre = self.grille[i_0 + i][j_0 + j]
                if chiffre > 0:
                    possible[chiffre] = False
        return [chiffre for chiffre in range(1, 10) if possible[chiffre]]
    
    def bon_choix(self):
        mini = 10
        choix = None
        for i in range(9):
            for j in range(9):
                if self.grille[i][j] == 0:
                    q = len(self.chiffres_possibles(i, j))
                    if q < mini:
                        mini = q
                        choix = (i, j)
        return choix


# Tests

diabolique = Sudoku('37...92....6.....55..27..4.7..6....2....9....6....3..7.6..28..44.....1....91...56')

possibles = [
    None,
    [1, 2, 4, 8, 9],
    [1, 8],
    None,
    None,
    None,
    [3, 7, 9],
    [2, 3, 7, 8, 9],
    None,
]

for k in range(9):
    if possibles[k] is not None:
        assert diabolique.chiffres_possibles(k, k) == possibles[k]

assert diabolique.bon_choix() == (6, 0)

