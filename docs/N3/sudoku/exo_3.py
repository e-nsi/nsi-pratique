class Sudoku:
    """Classe pour le puzzle Sudoku"""

    def __init__(self, texte):
        self.grille = ...

    def est_resolu(self):
        ...
        return ...
    
    def chiffres_possibles(self, i, j):
        ...
    
    def bon_choix(self):
        ...

    def solution(self):
        ...



# Tests

diabolique = Sudoku('37...92....6.....55..27..4.7..6....2....9....6....3..7.6..28..44.....1....91...56')

assert diabolique.solution()

solution = [
    [3, 7, 4, 8, 5, 9, 2, 6, 1],
    [9, 2, 6, 3, 4, 1, 8, 7, 5],
    [5, 1, 8, 2, 7, 6, 3, 4, 9],
    [7, 3, 1, 6, 8, 5, 4, 9, 2],
    [8, 4, 5, 7, 9, 2, 6, 1, 3],
    [6, 9, 2, 4, 1, 3, 5, 8, 7],
    [1, 6, 7, 5, 2, 8, 9, 3, 4],
    [4, 5, 3, 9, 6, 7, 1, 2, 8],
    [2, 8, 9, 1, 3, 4, 7, 5, 6],
]

for i in range(9):
    for j in range(9):
        assert diabolique.grille[i][j] == solution[i][j]

