class Sudoku:
    """Classe pour le puzzle Sudoku"""

    def chiffre(caractere):
        """Renvoie le chiffre associé à la représentation textuelle"""
        if caractere == '.':
            return 0
        else:
            return int(caractere)

    def ligne(k):
        """Renvoie toutes les coordonnées de la ligne k"""
        return [(k, j) for j in range(9)]

    def colonne(k):
        """Renvoie toutes les coordonnées de la colonne j"""
        return [(i, k) for i in range(9)]

    def region(k):
        """Renvoie toutes les coordonnées de la region n°k"""
        i, j = k - k%3, 3 * (k % 3)
        # (i, j) est désormais au coin supérieur gauche de sa région
        return [(i + di, j + dj) for di in range(3) for dj in range(3)]

    def num_region(i, j):
        """Renvoie le numéro de la région de (i, j)"""
        return i - i % 3 + j // 3
    
    def __init__(self, texte):
        self.grille = [
            [Sudoku.chiffre(texte[9*i + j]) for j in range(9)]
                for i in range(9)
        ]
    
    def est_resolu(self):
        for zone in [Sudoku.ligne, Sudoku.colonne, Sudoku.region]:
            for k in range(9):
                vu = [False] * 10
                for (i, j) in zone(k):
                    x = self.grille[i][j]
                    if vu[x]:
                        return False
                    vu[x] = True
                if vu != [False] + [True] * 9:
                    return False
        return True
    
    def __repr__(self):
        sortie = "Sudoku('"
        for i in range(9):
            for j in range(9):
                sortie += ".123456789"[self.grille[i][j]]
        return sortie + "')"

    def __str__(self):
        barre = "+---+---+---+\n"
        sortie = ""
        for i in range(9):
            if i % 3 == 0:
                sortie += barre
            for j in range(9):
                if j % 3 == 0:
                    sortie += "|"
                sortie += " 123456789"[self.grille[i][j]]
            sortie += "|\n"
        return sortie + barre

    def chiffres_possibles(self, i_0, j_0):
        possible = [True] * 10
        for i, j in ( Sudoku.ligne(i_0) + Sudoku.colonne(j_0)
            + Sudoku.region(Sudoku.num_region(i_0, j_0))):
                    chiffre = self.grille[i][j]
                    if chiffre > 0:
                        possible[chiffre] = False
        return [chiffre for chiffre in range(1, 10) if possible[chiffre]]
    
    def bon_choix(self):
        mini = 10
        choix = None
        for i in range(9):
            for j in range(9):
                if self.grille[i][j] == 0:
                    q = len(self.chiffres_possibles(i, j))
                    if q < mini:
                        mini = q
                        choix = (i, j)
        return choix

    def solution(self):
        choix = self.bon_choix()
        if choix is None:  # la grille est remplie
            return self.est_resolu()
        i_0, j_0 = choix
        for chiffre in self.chiffres_possibles(i_0, j_0):
            self.grille[i_0][j_0] = chiffre
            if self.solution():
                return True
        self.grille[i_0][j_0] = 0
        return False


# Tests

diabolique = Sudoku('37...92....6.....55..27..4.7..6....2....9....6....3..7.6..28..44.....1....91...56')

assert diabolique.solution()

solution = [
    [3, 7, 4, 8, 5, 9, 2, 6, 1],
    [9, 2, 6, 3, 4, 1, 8, 7, 5],
    [5, 1, 8, 2, 7, 6, 3, 4, 9],
    [7, 3, 1, 6, 8, 5, 4, 9, 2],
    [8, 4, 5, 7, 9, 2, 6, 1, 3],
    [6, 9, 2, 4, 1, 3, 5, 8, 7],
    [1, 6, 7, 5, 2, 8, 9, 3, 4],
    [4, 5, 3, 9, 6, 7, 1, 2, 8],
    [2, 8, 9, 1, 3, 4, 7, 5, 6],
]

for i in range(9):
    for j in range(9):
        assert diabolique.grille[i][j] == solution[i][j]
