---
author: Franck Chambon
title: Sudoku
tags:
  - 9-maxi
forum: https://mooc-forums.inria.fr/moocnsi/t/n3-sudoku/7764
---

# Résolution d'un Sudoku

!!! info "Le Sudoku"
    Le sudoku [^sudoku], est un jeu en forme de grille défini en 1979 par l'Américain Howard Garns, mais inspiré du carré latin, ainsi que du problème des 36 officiers du mathématicien suisse Leonhard Euler.

    [^sudoku]: :material-wikipedia: [Sudoku](https://fr.wikipedia.org/wiki/Sudoku)

    Le but du jeu est de remplir la grille avec une série de chiffres (ou de lettres ou de symboles) tous différents, qui ne se trouvent jamais plus d'une fois sur une même **ligne**, dans une même **colonne** ou dans une même **région**. Les symboles sont des chiffres allant de 1 à 9, les régions étant alors des carrés de 3 × 3. Quelques symboles sont déjà disposés dans la grille, ce qui autorise une résolution progressive du problème complet.

    TODO : image d'une grille

## Représentations d'un Sudoku

Un sudoku peut avoir une représentation textuelle telle que 

```python
sudoku_texte = '5...8..49...5...3..673....115..........2.8..........187....415..3...2...49..5...3'
```

ou bien sous une forme de tableau bi-dimensionnel tel que

```python
sudoku_tab = [
    [5, 0, 0, 0, 8, 0, 0, 4, 9],
    [0, 0, 0, 5, 0, 0, 0, 3, 0],
    [0, 6, 7, 3, 0, 0, 0, 0, 1],
    [1, 5, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 2, 0, 8, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 1, 8],
    [7, 0, 0, 0, 0, 4, 1, 5, 0],
    [0, 3, 0, 0, 0, 2, 0, 0, 0],
    [4, 9, 0, 0, 5, 0, 0, 0, 3],
]
```

- La représentation textuelle est une chaine de caractères de longueur 81, ne comportant que les caractères de `'.0123456789'` ; le `'.'` symbolise l'absence de chiffre.
- La représentation sous forme de tableau est une liste de 9 lignes, chacune étant une liste de 9 entiers de 0 à 9 ; le 0 représente l'absence de chiffre.


### Interface de la classe `Sudoku`

On souhaite disposer d'une classe `Sudoku` avec les éléments d'interface suivants :

- `__init__(self, texte)` : constructeur à partir de la représentation textuelle.
- `grille` : la représentation bi-dimensionnelle.
- `__repr__(self)` : pour un affichage console du genre `#!py Sudoku('<représentation textuelle>')`
- `__str__(self)` : pour un affichage console à destination des humains. Voir l'exemple.
- `est_resolu(self)` : pour déterminer si le sudoku est correctement résolu.

On souhaite que la représentation interne soit sous forme de tableau bi-dimensionnel.

La représentation textuelle n'est pas à conserver, mais sera retrouvée via la méthode `__repr__`

### Affichage d'un Sudoku

Pour cette première partie, on souhaite un affichage avec un style rétro, en mode texte, d'une grille de Sudoku.

```python
class Sudoku:
    # À compléter

# Tests

diabolique = Sudoku(
    '37...92....6.....55..27..4.7..6....2....9....6....3..7.6..28..44.....1....91...56'
)

diabolique_tab = [
    [3, 7, 0, 0, 0, 9, 2, 0, 0],
    [0, 0, 6, 0, 0, 0, 0, 0, 5],
    [5, 0, 0, 2, 7, 0, 0, 4, 0],
    [7, 0, 0, 6, 0, 0, 0, 0, 2],
    [0, 0, 0, 0, 9, 0, 0, 0, 0],
    [6, 0, 0, 0, 0, 3, 0, 0, 7],
    [0, 6, 0, 0, 2, 8, 0, 0, 4],
    [4, 0, 0, 0, 0, 0, 1, 0, 0],
    [0, 0, 9, 1, 0, 0, 0, 5, 6],
]

for i in range(9):
    # chaque ligne contient les mêmes valeurs
    assert diabolique.grille[i] == diabolique_tab[i]

assert diabolique.est_resolu() is False

assert repr(diabolique) == "Sudoku('37...92....6.....55..27..4.7..6....2....9....6....3..7.6..28..44.....1....91...56')"

print(diabolique)  # utilise la méthode `__str__`
```

```output
+---+---+---+
|37.|..9|2..|
|..6|...|..5|
|5..|27.|.4.|
+---+---+---+
|7..|6..|..2|
|...|.9.|...|
|6..|..3|..7|
+---+---+---+
|.6.|.28|..4|
|4..|...|1..|
|..9|1..|.56|
+---+---+---+
```

## I - Début de l'exercice

!!! question "Partie 1"
    Compléter la classe `#!py Sudoku`

    - Il sera nécessaire de compléter la méthode `#!py __init__` pour aborder la partie 2.
    - Il sera nécessaire de compléter la méthode `#!py est_resolu` pour aborder la partie 3.

{{ IDE('exo_1') }}

!!! abstract "Aides"
    1. Commencer par la création de la `grille`.
    2. Poursuivre avec la méthode `est_resolu` qui s'appuie sur `#!py self.grille`
    3. (facultatif) Pour la méthode `#!py __repr__`, il ne reste qu'à placer les chiffres et les points dans la sortie.
    4. (facultatif) Pour la méthode `#!py __str__`, les barres horizontales sont déjà gérées. À vous de gérer les autres lignes.
    


## II - Chiffres possibles dans une case

!!! info "Partie 1 incomplète ?"
    Il suffit d'avoir complété correctement la méthode `#!py __init__` pour faire la partie 2 de cet exercice.

Si `#!py self.grille[i][j]` vaut `0`, on souhaite obtenir avec la méthode `chiffres_possibles` la liste ordonnée des chiffres possibles à placer dans `#!py self.grille[i][j]`, pour `0 <= i < 9` et `0 <= j < 9`.

Pour un `sudoku` non rempli, on souhaite également obtenir un couple `(i, j)`

- tel que `#!py sudoku.grille[i][j]` vaut `0`,
- tel que `sudoku.chiffres_possibles(i, j)` soit minimal parmi les tels `(i, j)`,
- n'importe lequel `(i, j)`, en cas d'égalité.

La méthode `bon_choix` renverra un tel couple.

!!! question "Partie 2"
    Compléter la classe `#!py Sudoku`

    - Il sera nécessaire de valider entièrement cette partie pour aborder la partie 3.

{{ IDE('exo_2') }}


## III - Résolution récursive

!!! info "Partie 1 et 2 incomplètes ?"
    Pour aborder cette partie 3, il faut avoir complété correctement les méthodes
    
    - `#!py __init__`
    - `#!py est_resolu`
    - `#!py chiffres_possibles`
    - `#!py bon_choix`

On propose la méthode `solution` qui permet de déterminer si un sudoku peut être résolu et, dans l'affirmative, le résout en place.

1. Si la grille est remplie,
    - tester si elle est correctement résolue,
    - renvoyer un booléen en conséquence.
2. Sinon,
    - chercher un bon choix `(i, j)` pour y tester les chiffres possibles.
    - Pour chaque chiffre possible,
        - modifier la grille en conséquence,
        - de manière récursive, déterminer si le Sudoku ainsi modifié peut être résolu.
        - S'il est résolu, renvoyer `True`
    - Replacer un `0` en `(i, j)` pour annuler le test infructueux.
    - Renvoyer `False` pour indiquer que toutes les possibilités ont échouées.

!!! question "Partie 3"
    Compléter la classe `#!py Sudoku`

{{ IDE('exo_3') }}


## IV - Performances

L'algorithme proposé est, ici, assez performant.

Retrouvez en commentaire, après avoir résolu cette partie 3, des indications pour améliorer encore les performances.
