La version récursive est certainement plus dûre, on peut faire plusieurs
schémas avec des arbres complets pour en déduire la relation de récurrence
suivante :

$$
C_0 = 1 \textrm{ et } C_{n+1} = \sum_{i= 0}^n C_i C{n - i} \textrm{ pour } n
\leqslant 0
$$
