ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def cesar(message, decalage):
    resultat = ''
    for caractere in message:
        if caractere in ALPHABET:
            indice = (ALPHABET.index(caractere) + decalage) % 26
            resultat = resultat + ALPHABET[indice]
        else:
            resultat = resultat + caractere
    return resultat

