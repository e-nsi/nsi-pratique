# tests

assert disque_est_dans_rectangle(
    x=30, y=53, r=1,
    x_min=10, y_min=50, x_max=80, y_max=60
), "Erreur"

assert not disque_est_dans_rectangle(
    x=30, y=53, r=10,
    x_min=10, y_min=50, x_max=80, y_max=60
), "Erreur"

assert not disque_est_dans_rectangle(
    x=0, y=53, r=1,
    x_min=10, y_min=50, x_max=80, y_max=60
), "Erreur"


assert nb_disques_dans_rectangle(((10, 50), (80, 60)), []) == 0

assert nb_disques_dans_rectangle(((10, 50), (80, 60)),
     [(30, 53, 1), (30, 53, 10), (0, 53, 1)]) == 1

# autres tests

assert disque_est_dans_rectangle(
    x=3.0, y=5.3, r=0.1,
    x_min=1.0, y_min=5.0, x_max=8.0, y_max=6.0
), "Erreur"

assert not disque_est_dans_rectangle(
    x=3.0, y=5.3, r=1.0,
    x_min=1.0, y_min=5.0, x_max=8.0, y_max=6.0
), "Erreur"

assert not disque_est_dans_rectangle(
    x=0.0, y=5.3, r=0.1,
    x_min=1.0, y_min=5.0, x_max=8.0, y_max=6.0
), "Erreur"


assert nb_disques_dans_rectangle(((1.0, 5.0), (8.0, 6.0)), []) == 0

assert nb_disques_dans_rectangle(((1.0, 5.0), (8.0, 6.0)),
     [(3.0, 5.3, 0.1), (3.0, 5.3, 1.0), (0.0, 5.3, 0.1)]) == 1

