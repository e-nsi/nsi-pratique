#!/bin/sh

if [ $# = 0 ]
then
    echo "Pour transférer un niveau de nsi-pratique vers pratique."
    echo "Usage :"
    echo "transferer_exo.sh mon_exo numero"
    echo ""
    echo "Par exemple :"
    echo "transferer_exo.sh N1/mon_exo 652"
    echo "va transférer N1/mon_exo vers N1/652-mon_exo dans pratique"
    echo "Il faut être dans le dossier \"docs\" pour le lancer"
    exit 1
fi

#nom_exo=$(basename `realpath $1`)
#nom_complet_niveau=$(dirname `realpath $1`)
#nom_niveau=$(basename $nom_complet_niveau)
nom_exo=$(basename $1)
nom_complet_niveau=$(dirname $1)
nom_niveau=$(basename $nom_complet_niveau)


if [ $# = 1 ]
then
    echo "Il faut donner un numéro pour l'exo"
    exit 1
fi

numero=$2 #ça va probablement changer et être géré autrement

nom_depot_final="pratique"
docs_depot_final="$nom_depot_final/docs" # adresse relative

nom_depot_devel="nsi-pratique"
docs_depot_devel="$nom_depot_devel/docs"
#docs_depot_devel=`dirname \`realpath $0\`` # adresse absolue

# 0) On garde les tags
# Pour l'instant, c'est manuel
#pad="000"
#numero=`echo ${numero}${pad:${#numero}} | cut -c -3`


# on vérifie que tout va bien et qu'il n'y a pas de fichiers inutiles
#cd $docs_depot_devel
git pull
./verif_niveau.sh "$nom_niveau/$nom_exo"
if [ $? -ne 0 ]
then
    echo "La vérification n'est pas passée"
    exit 1
fi

nb_fichiers_pas_dans_git=`git ls-files -o "$nom_niveau/$nom_exo" | wc -l`
if [ $nb_fichiers_pas_dans_git -ne 0 ]
then
    echo "Il y a des fichiers dans le dossier qui ne sont pas dans git"
    git ls-files -o "$nom_niveau/$nom_exo"
    exit 1
fi

# 1) On copie dans pratique en changeant le numéro
cp -r "$nom_niveau/$nom_exo" "../../$docs_depot_final/$nom_niveau/$numero-$nom_exo"

# 2) On ajoute le hashage
# on passe dans le depot final
cd "../../$docs_depot_final"
git pull
git add "$nom_niveau/$numero-$nom_exo"
git commit -m "ajout de $nom_niveau/$numero-$nom_exo"
#./ajout_hashage.sh "$nom_niveau/$numero-$nom_exo"

# 3) On ajoute à git dans pratique
#git pull
#git add $nom_niveau/$numero-$nom_exo
#git commit -m "ajout du hashage dans $nom_niveau/$numero-$nom_exo"
git push

# 4) On ajoute dans _redirect
# on revient dans le dossier de départ
cd "../../$docs_depot_devel"
git pull
# 5) On supprime de nsi-pratique (dans git)
git rm -r "$nom_niveau/$nom_exo"
git commit -m "suppression après transfert de $nom_niveau/$nom_exo"
git push

